# Changelog


## Current development version (unreleased)

### Added
-

### Changed
-

### Deprecated
-

### Removed
-

### Fixed
-


## 0.1.1 (2021-11-18)

### Fixed
- Issue #113: proba_repro and life_expectancy_repro are incorrectly computed
- Issue #111: plot.trajectory raises KeyError: 'Classes'.


## 0.1.0 (2021-10-19)

First beta release.

