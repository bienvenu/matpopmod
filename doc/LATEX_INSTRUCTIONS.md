# Compiling to LaTeX

This file contains instructions aimed at users who want to compile the
documentation to LaTeX. Note that the LaTeX version of the documentation is
not a priority of the development team of matpopmod.


## Motivation 

Customizing the .tex generated by Sphinx is challenging -- and often comes
at the price of polluting the .rst source files. As a result, it is sometimes
easier, e.g, to edit the .tex files manually before compiling the pdf.
Below is a list of modifications that should be made to the file matpopmod.tex
before compiling the PDF that will be hosted on the Zenodo repository.


## Modifications that need to be made

### Add `doc/_static/logo.pdf` to `public`

After creating the file `matpopmod.tex` with Sphinx, but before compiling it
to PDF with pdflatex, manually copy the file `doc/_static/logo.pdf` to
the folder `public` containing `matpopmod.tex`.

This is because Sphinx is not aware of the existence of this file (since
we use the SVG version of the logo in the html version of the documentation)
and therefore does not copy it to public. Ideally, there should be a way to
automatize this, e.g, by making Sphinx aware of the existence of this
file or by adding the copy command to to the makefile generated by Sphinx in
`public`.


### Image gallery in Chapter 1

Remove the borders of the cells and center their text elements in the
image gallery at the beginning of Chapter 1, and replace the `\\` separating
the first from the second row by `\tabularnewline`. This amounts to replacing
the code of the table by:

```
\begin{tabulary}{\linewidth}[t]{TTT}

\begin{sphinxfigure-in-table}
\centering

\noindent\sphinxincludegraphics{{01}.pdf}
\end{sphinxfigure-in-table}\relax

\centering
Life cycle graph
&

\begin{sphinxfigure-in-table}
\centering

\noindent\sphinxincludegraphics{{02}.pdf}
\end{sphinxfigure-in-table}\relax

\centering
Eigenvalues
&

\begin{sphinxfigure-in-table}
\centering

\noindent\sphinxincludegraphics{{03}.pdf}
\end{sphinxfigure-in-table}\relax

\centering
Deterministic
trajectory
\tabularnewline

\begin{sphinxfigure-in-table}
\centering

\noindent\sphinxincludegraphics{{04}.pdf}
\end{sphinxfigure-in-table}\relax

\centering
Second\sphinxhyphen{}order
oscillations
&

\begin{sphinxfigure-in-table}
\centering

\noindent\sphinxincludegraphics{{05}.pdf}
\end{sphinxfigure-in-table}\relax

\centering
Stochastic
trajectories
&

\centering
Individual\sphinxhyphen{}based
simulations \sphinxstyleemphasis{(v0.2.0)}\\

\end{tabulary}
```


### Image gallery at the beginning of Section 2.4

Remove the borders of the table at the beginning of the section labeled
`quickstart:plotting-trajectories`.


### Rename the pdf

Rename the compiled pdf `documentation.pdf` and add it to the zip archive.

