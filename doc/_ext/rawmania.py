from docutils import nodes
from docutils.parsers.rst import Directive
from docutils.parsers.rst import directives, roles, states


def ParametrizedHeader(html_level, latex_level):

  class Header(Directive):
    required_arguments = 2
    optional_arguments = 0
    final_argument_whitespace = True
    has_content = False

    def run(self):
      if not self.state.document.settings.raw_enabled:
        raise self.warning(f"{self.name} directive disabled.")
      idsec, namesec = self.arguments[0], self.arguments[1]
      latex_namesec = namesec.replace("<strong>", r"\textbf{").replace("</strong>", "}")
      source_and_line = self.state_machine.get_source_and_line(self.lineno)
      html_text = f'\n<{html_level} id="{idsec}">{namesec}</{html_level}>\n'
      raw_node_html = nodes.raw("", html_text, format = "html")
      raw_node_html.source, raw_node_html.line = source_and_line
      latex_text = ("\n\\" + latex_level + "*{" + latex_namesec +
                                           "} \\label{" + idsec + "}\n")
      raw_node_latex = nodes.raw("", latex_text, format = "latex")
      raw_node_latex.source, raw_node_latex.line = source_and_line
      return [raw_node_html, raw_node_latex]

  return Header


h1 = ParametrizedHeader("h1", "chapter")
h2 = ParametrizedHeader("h2", "section")
h3 = ParametrizedHeader("h3", "subsection")
h4 = ParametrizedHeader("h4", "subsubsection")
h5 = ParametrizedHeader("h5", "paragraph")
h6 = ParametrizedHeader("h6", "paragraph")


class OpenLink(Directive):
  required_arguments = 1
  optional_arguments = 0
  final_argument_whitespace = False
  has_content = False

  def run(self):
    if not self.state.document.settings.raw_enabled:
      raise self.warning(f"{self.name} directive disabled.")
    target = self.arguments[0]
    source_and_line = self.state_machine.get_source_and_line(self.lineno)
    html_text = f'<a href="{target}">'
    raw_node_html = nodes.raw("", html_text, format = "html")
    raw_node_html.source, raw_node_html.line = source_and_line
    return [raw_node_html]


class CloseLink(Directive):
  required_arguments = 0
  optional_arguments = 0
  final_argument_whitespace = False
  has_content = False

  def run(self):
    if not self.state.document.settings.raw_enabled:
      raise self.warning(f"{self.name} directive disabled.")
    source_and_line = self.state_machine.get_source_and_line(self.lineno)
    raw_node_html = nodes.raw("", "</a>", format = "html")
    raw_node_html.source, raw_node_html.line = source_and_line
    return [raw_node_html]


def setup(app):
  app.add_directive("h1", h1)
  app.add_directive("h2", h2)
  app.add_directive("h3", h3)
  app.add_directive("h4", h4)
  app.add_directive("h5", h5)
  app.add_directive("h6", h6)
  app.add_directive("a", OpenLink)
  app.add_directive("a-close", CloseLink)
  return {
    'version': '1.0',
    'parallel_read_safe': True,
    'parallel_write_safe': True,
  }
