matpopmod.collapsing
====================


.. automodule:: matpopmod.collapsing

  .. autofunction:: individualistic_collapsing
  .. autofunction:: genealogical_collapsing
  .. autofunction:: collapsing_matrix

