matpopmod.compadre
==================

.. automodule:: matpopmod.compadre

.. autoclass::  MPMCollection

.. h2:: module-functions Functions implemented in this module

.. autofunction:: convert
.. autofunction:: fetch
.. autofunction:: filter_collection
.. autofunction:: load
.. autofunction:: merge
.. autofunction:: save 

.. h2:: invali-MPM Invalid MPMs

.. autoclass::  InvalidMPM

.. h2:: known-versions Known versions of COM[P]ADRE

If a version of COMADRE / COMPADRE is not in the following lists, it means that
it has not been tested with matpopmod; not that it cannot be used.

.. autodata:: COMPADRE_VERSIONS
    :annotation:
.. autodata:: COMADRE_VERSIONS
    :annotation:

.. h2:: compadre-exceptions List of problematic COM[P]ADRE versions

As of 24/09/2021, the following versions of COMPADRE/COMADRE have been
observed to cause some issues with matpopmod. All other versions worked
as expected out of the box -- but that can depend on the version of
R used to convert the databases from RData to JSON. Here, R 4.1.1 was used.
Using older versions of R, you might get the following error with
recent versions of the databases:

.. code-block:: R

  ReadItem: unknown type 115, perhaps written by later version of R

Do not hesitate to contact us `by email <index.html#authors>`_ or
`on GitLab <https://gitlab.com/bienvenu/matpopmod/-/issues>`_ to report other
problems.

.. list-table::
   :widths: 20 20 60
   :header-rows: 1

   * - Database
     - Version
     - Comments
   * - COMADRE
     - 4.20.8.0
     - The file
       `COMADRE_v.4.20.8.0.RData <https://www.compadre-db.org/Data/Download/COMADRE_v.4.20.8.0.RData>`_
       hosted on compadre-db.org appears to be empty.
   * - COMPADRE
     - 5.0.1
     - Installing the R package Rcompadre should fix the issue.
   * - COMADRE
     - 3.0.1
     - Installing the R package Rcompadre should fix the issue.


.. include:: ../compadre.inc

