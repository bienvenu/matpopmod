matpopmod.examples 
==================

.. automodule:: matpopmod.examples

  .. h3:: complete-list Complete list of models

  - :data:`orcinus_orca`
  - :data:`bernardelli_beetle`
  - :data:`passerine_postbreeding`
  - :data:`passerine_prebreeding`
  - :data:`dipsacus_sylvestris`
  - :data:`homo_sapiens_USA`
  - :data:`thalia_democratica`
  - :data:`astrocaryum_mexicanum`
  - :data:`second_order_oscillations`

  .. autodata:: orcinus_orca
    :annotation:

  .. autodata:: bernardelli_beetle 
    :annotation:

  .. autodata:: passerine_postbreeding
    :annotation:

  .. autodata:: passerine_prebreeding
    :annotation:

  .. autodata:: dipsacus_sylvestris
    :annotation:

  .. autodata:: homo_sapiens_USA
    :annotation:

  .. autodata:: thalia_democratica
    :annotation:

  .. autodata:: astrocaryum_mexicanum
    :annotation:

  .. autodata:: second_order_oscillations
    :annotation:

