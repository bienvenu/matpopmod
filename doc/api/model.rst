matpopmod.model
===============

.. automodule:: matpopmod.model

  In what follows, we detail how to create :class:`MPM` objects and we
  give the complete list of descriptors that can be computed with them.

  .. autoclass:: MPM

    Finally, each model has a :attr:`metadata` attribute. It is simply a
    dictionary that can be used to store relevant information about the model.


    .. h2:: list-descriptors Complete list of available descriptors

    Unless a specific reference is given, the formulas used are classic
    and can be found in textbooks such as [Casw00]_ or [KeCa05]_.

    .. raw:: html

      <ol type = "I">
      <li><a href="#sec-descr-A">Descriptors of the projection matrix</a>
        <ol type = "1">
          <li><a href="#subsec-struct-prop">Structural properties</a>
            <ol type = "a">
              <li><a href="#subsubsec-matrix-A">The matrix <strong>A</strong></a></li>
              <li><a href="#subsubsec-irreducibility">Irreducibility and related quantities</a></li>
              <li><a href="#subsubsec-aperiodicity">Aperiodicity and related quantities</a></li>
              <li><a href="#subsubsec-primitivity">Primitivity and quasi-primitivity</a></li>
            </ol>
          </li>
          <li><a href="#subsec-eigen">Eigen-elements and related quantities</a>
            <ol type = "a">
              <li><a href="#subsubsec-asymp">Asymptotic dynamics</a></li>
              <li><a href="#subsubsec-second-order">Second order dynamics</a></li>
              <li><a href="#subsubsec-full-spec">Other eigen-elements</a></li>
              <li><a href="#subsubsec-derivatives">Derivatives (sensitivities and elasticities)</a></li>
            </ol>
          </li>
          <li><a href="#subsec-genealogical">The genealogical Markov chain</a>
            <ol type = "a">
              <li><a href="#subsubsec-matrix-P">Transition matrix and stationary distribution</a></li>
              <li><a href="#subsubsec-stats-P">Entropy rate and other statistics</a></li>
            </ol>
          </li>
        </ol>
      </li>
      <li><a href="#sec-descr-S-F">Descriptors of the survival and fertility matrices</a>
        <ol type = "1">
          <li><a href="#subsec-S-F">The
            <strong>A</strong> = <strong>S</strong> + <strong>F</strong> decomposition</a>
            <ol type = "a">
              <li><a href="#subsubsec-matrices-S-F">Matrices</a></li>
              <li><a href="#subsubsec-classic">Leslie and Usher models</a></li>
              <li><a href="#subsubsec-classes">Newborn and reproductive classes</a></li>
              <li><a href="#subsubsec-steady-state">Steady state distributions</a></li>
              <li><a href="#subsubsec-excesses">Survival and fertility excesses</a></li>
            </ol>
          </li>
          <li><a href="#subsec-biological-descr">Biological descriptors</a>
            <ol type = "a">
              <li><a href="#subsubsec-repro">Reproductive output</a></li>
              <li><a href="#subsubsec-R0">Net reproductive rate</a></li>
              <li><a href="#subsubsec-generation-time">Generation time</a></li>
              <li><a href="#subsubsec-life-expectancy">Life expectancy</a></li>
              <li><a href="#subsubsec-age-structure">Age structure</a></li>
              <li><a href="#subsubsec-entropies">Entropies</a></li>
            </ol>
          </li>
        </ol>
      </li>
      <li><a href="#sec-functions">Functions of the initial population</a>
        <ol type = "1">
          <li><a href="#subsec-dist-stable-pop">Distance to the stable population</a></li>
          <li><a href="#subsec-trajectories">Trajectories</a></li>
        </ol>
      </li>
      </ol>


    .. h2:: sec-descr-A I.  Descriptors of the projection matrix
   
    .. h3:: subsec-struct-prop I.1.  Structural properties

    .. h4:: subsubsec-matrix-A I.1.a.  The matrix <strong>A</strong>
    .. autoattribute:: A
    .. autoattribute:: dim

    .. h4:: subsubsec-irreducibility I.1.b.  Irreducibility and related quantities
    .. autoattribute:: irreducible
    .. autoattribute:: irreducible_components
    .. autoattribute:: normal_form 
    .. autoattribute:: quasi_irreducible 

    .. h4:: subsubsec-aperiodicity I.1.c.  Aperiodicity and related quantities
    .. autoattribute:: periods
    .. autoattribute:: index_of_imprimitivity
    .. autoattribute:: aperiodic

    .. h4:: subsubsec-primitivity I.1.d.  Primitivity and quasi-primitivity
    .. autoattribute:: primitive
    .. autoattribute:: quasi_primitive


    .. h3:: subsec-eigen I.2.  Eigen-elements and related quantities

    .. h4:: subsubsec-asymp I.2.a.  Asymptotic dynamics
    .. autoattribute:: lmbd 
    .. autoattribute:: w 
    .. autoattribute:: v 

    .. h4:: subsubsec-second-order I.2.b.  Second order dynamics
    .. autoattribute:: damping_ratio
    .. autoattribute:: second_order_period 

    .. h4:: subsubsec-full-spec I.2.c.  Other eigen-elements
    .. autoattribute:: eigenvalues
    .. autoattribute:: right_eigenvectors
    .. autoattribute:: left_eigenvectors

    .. h4:: subsubsec-derivatives I.2.d.  Derivatives (sensitivities and elasticities)
    .. autoattribute:: sensitivities
    .. autoattribute:: elasticities


    .. h3:: subsec-genealogical I.3.  The genealogical Markov chain

    .. h4:: subsubsec-matrix-P I.3.a  Transition matrix and stationary distribution
    .. autoattribute:: P
    .. autoattribute:: pi

    .. h4:: subsubsec-stats-P I.3.b  Entropy rate and other statistics
    .. autoattribute:: entropy_rate
    .. autoattribute:: hitting_times
    .. autoattribute:: kemeny_constant

 
    .. h2:: sec-descr-S-F II.  Descriptors of the survival and fertility matrices

    .. h3:: subsec-S-F II.1.  The <strong>A</strong> = <strong>S</strong> + <strong>F</strong> decomposition

    .. h4:: subsubsec-matrices-S-F II.1.a  Matrices
    .. autoattribute:: split
    .. autoattribute:: S
    .. autoattribute:: F
    .. autoattribute:: mixed_transitions
    .. autoattribute:: fundamental_matrix
    .. autoattribute:: Ps
    .. autoattribute:: Pf
    .. autoattribute:: fundamental_matrix_Ps

    .. h4:: subsubsec-classic II.1.b  Leslie and Usher models
    .. autoattribute:: leslie
    .. autoattribute:: usher
    .. autoattribute:: relabeled_leslie
    .. autoattribute:: relabeled_usher

    .. h4:: subsubsec-classes II.1.c  Newborn and reproductive classes
    .. autoattribute:: newborn_classes
    .. autoattribute:: unique_newborn_class
    .. autoattribute:: reproductive_classes
    .. autoattribute:: postreproductive_classes
    .. autoattribute:: proba_maturity

    .. h4:: subsubsec-steady-state II.1.d  Steady state distributions
    .. autoattribute:: proportion_newborns 
    .. autoattribute:: nu
    .. autoattribute:: class_of_birth
    .. autoattribute:: class_of_death 

    .. h4:: subsubsec-excesses II.1.d  Fertility and survival excesses
    .. autoattribute:: fertility_excess
    .. autoattribute:: survival_excess
    .. autoattribute:: theta

    .. h3:: subsec-biological-descr II.2.  Biological descriptors

    .. h4:: subsubsec-repro II.2.a  Reproductive output

    .. autoattribute:: G 
    .. autoattribute:: w_G
    .. autoattribute:: v_G
    .. autoattribute:: total_reproductive_output
    .. autoattribute:: proba_repro

    .. h4:: subsubsec-R0 II.2.b  Net reproductive rate
    .. autoattribute:: R0 
    .. autoattribute:: cohort_R0 

    .. h4:: subsubsec-generation-time II.2.c  Generation time
    .. autoattribute:: T_a
    .. autoattribute:: T_R0
    .. autoattribute:: T_G
    .. autoattribute:: mu1

    .. rst-class:: long-signature
    .. automethod:: mean_age_repro

    .. h4:: subsubsec-life-expectancy II.2.d  Life expectancy

    *Important remark: an unavoidable problem when using matrix population
    models to compute age-specific quantities is that the exactitude of
    the formulas depend on the specific assumptions made when building the
    model.*
    
    *For instance, in pre-breeding models, "newborns" are*
    :math:`1 - \varepsilon` *years old, with* :math:`\varepsilon` *small,
    whereas in post-breeding models they are* :math:`\varepsilon` *years old.
    Similarly, if an individual is alive at time t and not alive at time t + 1,
    then we know that it died between times t and t + 1; but we
    have no way of knowing when.*

    *As a result, when interpreting quantities such as the life expectancy or
    the mean age in a class, one should remember that "age" is counted
    in number of observations of the individuals, rather than absolute time.
    One may also want to add/subtract a correction term, e.g, based on
    knowledge on when deaths are most likely to occur in the projection
    interval.*
    
    .. automethod:: survivorship
    .. automethod:: class_survivorship
    .. autoattribute:: life_expectancy
    .. autoattribute:: remaining_life_expectancy
    .. autoattribute:: conditional_life_expectancy
    .. autoattribute:: life_expectancy_repro
    .. autoattribute:: remaining_life_expectancy_repro
    .. autoattribute:: variance_lifespan
    .. autoattribute:: variance_remaining_lifespan

    .. h4:: subsubsec-age-structure II.2.e  Age structure

    .. autoattribute:: mean_age_class
    .. autoattribute:: mean_age_population
    .. autoattribute:: variance_age_class
    .. autoattribute:: variance_age_population
    .. automethod:: age_specific_fertility
    .. autoattribute:: mean_age_maturity 
    .. autoattribute:: mean_age_first_repro

    .. h4:: subsubsec-entropies II.2.f  Entropies

    *See* :attr:`entropy_rate` *above for the entropy rate H of the
    genealogical Markov chain.*

    .. autoattribute:: population_entropy
    .. autoattribute:: birth_entropy
    .. autoattribute:: genealogical_entropy
    .. autoattribute:: lifetable_entropy

    .. h2:: sec-functions III.  Functions of the initial population

    .. h3:: subsec-dist-stable-pop III.1  Distance to the stable population

    .. automethod:: keyfitz_delta
    .. automethod:: cohen_D1
    .. automethod:: population_momentum

    .. h3:: subsec-trajectories III.2  Trajectories

    .. automethod:: trajectory

    .. rst-class:: long-signature
    .. automethod:: stochastic_trajectories


.. h2:: footnotes Footnotes

1. When we say that the population satisfies
   :math:`\mathbf{n}(t) \sim \mathbf{vn}(0) \lambda^t \mathbf{w}` for "any"
   initial population vector, in fact this is true if and only if
   :math:`\mathbf{vn}(0) \neq 0`. For instance,
   if :math:`\mathbf{n}(0)` were colinear with another right-eigenvector of the
   projection matrix -- say :math:`\mathbf{w}'` with associated eigenvalue
   :math:`\lambda'` -- then we would have
   :math:`\mathbf{n}(t) \sim \mathbf{n}(0) (\lambda')^t`.

   However, the set of vectors **x** such that **vx** = 0 is a vector space
   of dimension *n* - 1 in the *n*-dimensional space of population vectors.
   Thus, when choosing an initial population vector according to any
   "reasonable" probability distribution, the probability of getting a vector
   such that :math:`\mathbf{vn}(0) = 0` is equal to 0, just as there is a
   probability zero of falling exactly on a given line when throwing a point in
   the plane.  As a result, the case :math:`\mathbf{vn}(0) = 0` can be
   considered a degenerate case with no biological relevance.

2. The terminology regarding the quantities *H* and *S* has not been very
   consistent; in fact Demetrius himself has used both *population entropy* and
   *evolutionary entropy* to refer to either of the two.
   The convention we use here corresponds to a distinction explicitly made
   by Demetrius in a correspondence with FB, where he referred to *H* as
   the evolutionary entropy and to *S* as the population entropy.
   However, we favor the term *entropy rate* over evolutionary entropy because
   it is more explicit.

   While the definition and interpretation of *S* are very clear and natural
   in the context of Leslie models, this is not the case for general matrix
   population models. Based on the same correspondence with Demetrius,
   we adopt the point of view that *S* should be *defined* so as to ensure
   that :math:`S = H / T_a`. This, however, means that we have to renounce to
   the probabilistic interpretations that *S* has in the context of Leslie
   models.

.. |any*| replace::

   |anylatex| |anyhtml|

.. |anylatex| raw:: latex

  any\textsuperscript{\hyperref[footnotes]{1}}

.. |anyhtml| raw:: html

  any<a href="#footnotes">¹</a>


.. |entropy*| replace::

   |entropylatex|\ |entropyhtml|

.. |entropylatex| raw:: latex

  \textsuperscript{\hyperref[footnotes]{2}}

.. |entropyhtml| raw:: html

  <a href="#footnotes">²</a>

