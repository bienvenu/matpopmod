Documentation
=============


Click on the name of a module to see its complete documentation.


==============================   ===========
Module name                      Description
==============================   ===========
:mod:`~matpopmod.collapsing`     Model reduction by aggregation of classes.
:mod:`~matpopmod.compadre`       Interface to the COMPADRE/COMADRE databases. 
:mod:`~matpopmod.examples`       Selection of ready-made models. 
:mod:`~matpopmod.ibm`            Individual based model simulation and analysis.
:mod:`~matpopmod.kinship`        Computation of the kinship matrices. 
:mod:`~matpopmod.mathtools`      Mathematical functions, for advanced use. 
:mod:`~matpopmod.model`          Matrix population models and their biological descriptors.
:mod:`~matpopmod.plot`           Plotting utilities.
:mod:`~matpopmod.random`         Random matrix population model generation. 
:mod:`~matpopmod.trajectories`   High-level representation of trajectories.
:mod:`~matpopmod.utils`          *(internal module, not aimed at end users)*
==============================   ===========


.. toctree::
   :hidden:

   collapsing
   compadre
   examples
   ibm
   kinship
   mathtools
   model
   plot
   random
   trajectories
   utils

