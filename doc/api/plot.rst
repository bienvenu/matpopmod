matpopmod.plot
===============

.. automodule:: matpopmod.plot

  .. h2:: matplotlib-aliases Matplotlib aliases

  Below are aliases of a few `matplotlib <https://matplotlib.org>`_ functions,
  for convenience.

  .. function:: show

    Alias of :func:`matplotlib.pyplot.show`, to display the current figure.

  .. function:: savefig

    Alias of :func:`matplotlib.pyplot.savefig`, to save the current figure.


  .. h2:: matpopmod-plot Plotting functions

  .. rst-class:: long-signature
  .. autofunction:: life_cycle
    
    Here is an example that uses the default configuration:

    .. plot::
      :include-source:
           
      import matpopmod as mpm
      mpm.plot.life_cycle(mpm.examples.dipsacus_sylvestris)

    And here is an example that uses a custom one:

    .. plot::
      :include-source:
           
      mpm.plot.life_cycle(
        mpm.examples.dipsacus_sylvestris,
        node_labels = [
          "$S_1$: first year dormant seeds",
          "$S_2$: second year dormant seeds",
          "$R_1$: small rosettes",
          "$R_2$: medium rosettes",
          "$R_3$: large rosettes",
          "$F $: flowering plant"],
        show_legend = "center",
        fertilities = ":",
        node_colors = [
          "#606060",
          "#606060",
          "#A9A9A9",
          "#A9A9A9",
          "#A9A9A9",
          "#E8E8E8"],
        fill_nodes = True,
        node_fontsize = 14,
        title = "Life cycle of $Dipsacus$ $sylvestris$"
      )


  .. rst-class:: long-signature
  .. autofunction:: eigenvalues

    .. plot::
      :include-source:

      import matpopmod as mpm
      mpm.plot.eigenvalues(mpm.examples.dipsacus_sylvestris)

    And here are the same eigenvalues, but using a "log-polar" plot and
    a bit of customization:
   
    .. plot::
      :include-source:

      mpm.plot.eigenvalues(
        mpm.examples.dipsacus_sylvestris,
        log = True,
        grid_radius = 3, 
        grid_slices = 12,
        grid_circles = 5,
        style_eig = dict(color="C0", linewidth=1)
      )

  .. rst-class:: long-signature
  .. autofunction:: trajectory

    Here are a few examples of the type of plots that can be obtained for a
    given trajectory. All of these plots corresponds to the same trajectory,
    namely::

      dipsacus = matpopmod.examples.dipsacus_sylvestris
      traj = dipsacus.trajectory([0, 0, 0, 0, 0, 1], 20)
    
    Click on a thumbnail to see the corresponding code.

    .. rst-class:: image-gallery 
    
    +-------------------------------+-------------------------------+-------------------------------+
    |                               |                               |                               |
    |  .. a:: ../fig/doctraj01.html |  .. a:: ../fig/doctraj02.html |  .. a:: ../fig/doctraj03.html |
    |                               |                               |                               |
    |  .. rst-class:: glry          |  .. rst-class:: glry          |  .. rst-class:: glry          |
    |                               |                               |                               |
    |  .. plot:: fig/doctraj01.py   |  .. plot:: fig/doctraj02.py   |  .. plot:: fig/doctraj03.py   |
    |                               |                               |                               |
    |  .. a-close::                 |  .. a-close::                 |  .. a-close::                 |
    |                               |                               |                               |
    |  Population size,             |  Population size,             |  Class abundances,            |
    |  linear scale.                |  second order, log scale.     |  second order, rescaled.      |
    |                               |                               |                               |
    +-------------------------------+-------------------------------+-------------------------------+
    |                               |                               |                               |
    |  .. a:: ../fig/doctraj04.html |  .. a:: ../fig/doctraj05.html |  .. a:: ../fig/doctraj06.html |
    |                               |                               |                               |
    |  .. rst-class:: glry          |  .. rst-class:: glry          |  .. rst-class:: glry          |
    |                               |                               |                               |
    |  .. plot:: fig/doctraj04.py   |  .. plot:: fig/doctraj05.py   |  .. plot:: fig/doctraj06.py   |
    |                               |                               |                               |
    |  .. a-close::                 |  .. a-close::                 |  .. a-close::                 |
    |                               |                               |                               |
    |  Class abundances,            |  Class abundances,            |  Class abundances,            |
    |  log scale.                   |  stacked, custom window.      |  stacked, rescaled.           |
    |                               |                               |                               |
    +-------------------------------+-------------------------------+-------------------------------+



  .. rst-class:: long-signature
  .. autofunction:: multiple_trajectories

    Here are a few examples of the type of plots that can be obtained for a
    given trajectory. The following plots correspond to the same set of
    random trajectories, namely::

      import matpopmod as mpm
      dp = mpm.examples.dipsacus_sylvestris
      n0 = [0, 0, 0, 0, 0, 1]
      trajs = dp.stochastic_trajectories(n0, t_max=15, reps=1000)

    .. rst-class:: image-gallery 

    +-------------------------------+-------------------------------+-------------------------------+
    |                               |                               |                               |
    |  .. a:: ../fig/docmult01.html |  .. a:: ../fig/docmult02.html |  .. a:: ../fig/docmult03.html |
    |                               |                               |                               |
    |  .. rst-class:: glry          |  .. rst-class:: glry          |  .. rst-class:: glry          |
    |                               |                               |                               |
    |  .. plot:: fig/docmult01.py   |  .. plot:: fig/docmult02.py   |  .. plot:: fig/docmult03.py   |
    |                               |                               |                               |
    |  .. a-close::                 |  .. a-close::                 |  .. a-close::                 |
    |                               |                               |                               |
    |  Rescaled, linear scale.      |  Second order, log scale.     |  Centered, rescaled and       |
    |                               |                               |  log scale.                   |
    |                               |                               |                               |
    +-------------------------------+-------------------------------+-------------------------------+


  .. rst-class:: long-signature
  .. autofunction:: matrices
    
    Here is a showcase of the different plotting styles:

    .. rst-class:: image-gallery 
    
    +-------------------------------+-------------------------------+-------------------------------+
    |                               |                               |                               |
    |  .. a:: ../fig/docmat01.html  |  .. a:: ../fig/docmat02.html  |  .. a:: ../fig/docmat03.html  |
    |                               |                               |                               |
    |  .. rst-class:: glry          |  .. rst-class:: glry          |  .. rst-class:: glry          |
    |                               |                               |                               |
    |  .. plot:: fig/docmat01.py    |  .. plot:: fig/docmat02.py    |  .. plot:: fig/docmat03.py    |
    |                               |                               |                               |
    |  .. a-close::                 |  .. a-close::                 |  .. a-close::                 |
    |                               |                               |                               |
    |  Bubbles                      |  Heatmap                      |  Bars                         |
    |                               |                               |                               |
    +-------------------------------+-------------------------------+-------------------------------+

