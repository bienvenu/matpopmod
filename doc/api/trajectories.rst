matpopmod.trajectories
======================


.. automodule:: matpopmod.trajectories

  .. autoclass:: Trajectory

    .. h3:: basic-attributes Basic attributes of the trajectory 
    .. autoattribute:: timescale
    .. autoattribute:: Y
    .. autoattribute:: model
    .. autoattribute:: stochastic

    .. h3:: transfo-Y Transformations of the population vector
    .. autoattribute:: rescaled_Y
    .. autoattribute:: centered_Y
    .. autoattribute:: rescaled_centered_Y
    .. autoattribute:: second_order_Y
    .. autoattribute:: rescaled_second_order_Y

    .. h3:: other Other methods 
    .. automethod:: set_timescale

  .. h2:: trajectory-generation Trajectory generation
  .. rst-class:: long-signature
  .. autofunction:: compute_trajectory
  .. rst-class:: long-signature
  .. autofunction:: compute_stochastic_trajectories

