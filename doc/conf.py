# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html


# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
# sys.path.insert(0, os.path.abspath('.'))
import os
import sys
import matpopmod
sys.path.append(os.path.abspath("./_ext"))


# -- Project information -----------------------------------------------------

project = 'matpopmod'
copyright = '2021, François Bienvenu'
author = 'François Bienvenu'

# The full version, including alpha/beta/rc tags
release = matpopmod.__version__

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "matplotlib.sphinxext.plot_directive",
    "rawmania",
]

intersphinx_mapping = {
  'matplotlib': ('https://matplotlib.org/stable', None),
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# Document that contains the root toctree directive
root_doc = "contents"


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = 'alabaster'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_favicon = '_static/logo.svg'

html_theme_options = {
    'logo': 'logo.svg',
}


# -- Options for LaTeX output ------------------------------------------------
latex_maketitle = r'''
\makeatletter
\let\spx@tempa\relax
\ifHy@pageanchor\def\spx@tempa{\Hy@pageanchortrue}\fi
\hypersetup{pageanchor=false}% avoid duplicate destination warnings
\begin{titlepage}%
  \let\footnotesize\small
  \let\footnoterule\relax
  \noindent\rule{\textwidth}{1pt}\par
    \begingroup % for PDF information dictionary
     \def\endgraf{ }\def\and{\& }%
     \pdfstringdefDisableCommands{\def\\{, }}% overwrite hyperref setup
     \hypersetup{pdfauthor={\@author}, pdftitle={\@title}}%
    \endgroup
  \vspace{1cm}
  \begin{flushright}%
  \vspace{0.5cm}
   \includegraphics[width=0.3\linewidth]{logo.pdf}\par
    \py@HeaderFamily
    {\Huge \@title \par}
    {\itshape\LARGE Version\py@release\releaseinfo \par}
    \vfill
    {\LARGE
      \begin{tabular}[t]{c}
        \@author
      \end{tabular}
      \par}
    \vfill\vfill
    {\large
     \@date \par
     \vfill
     \py@authoraddress \par
    }%
  \end{flushright}%\par
  \@thanks
  \begin{sphinxadmonition}{note}{Note:}
  \sphinxAtStartPar
  This PDF was generated automatically and has not been proof-read.
  Its purpose is to archive the documentation of version\py@release\ of
  matpopmod. Unless you want to read the documentation of this specific
  version, we recommend consulting the online documentation of the
  latest version of matpopmod, available at
  \url{https://bienvenu.gitlab.io/matpopmod}.
  \end{sphinxadmonition}
  \end{titlepage}%
  \setcounter{footnote}{0}%
  \let\thanks\relax\let\maketitle\relax
  %\gdef\@thanks{}\gdef\@author{}\gdef\@title{}
  \if@openright\cleardoublepage\else\clearpage\fi
  \spx@tempa
\makeatother
'''

latex_elements = {
    'inputenc': '\\usepackage[utf8]{inputenc}',
    'fontenc': '\\usepackage[LGR,T1]{fontenc}',
    'extraclassoptions': 'openany,oneside',
    'releasename': '{}',
    'preamble': r"""
        \usepackage{newunicodechar}
        \newunicodechar{≤}{\ensuremath{\leq}}
        \newunicodechar{≥}{\ensuremath{\geq}}
      """,
    'maketitle': latex_maketitle,
    'printindex': '\\footnotesize\\raggedright\\printindex'
}

# -- Options for Plot directive ----------------------------------------------

plot_html_show_source_link = False

plot_rcparams = {'savefig.bbox': 'tight'}

plot_apply_rcparams = True

plot_pre_code = """
import matpopmod
import matpopmod as mpm
"""

plot_html_show_formats = False
