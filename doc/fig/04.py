import matpopmod as mpm

dipsacus = mpm.examples.dipsacus_sylvestris

traj = dipsacus.trajectory(
  [0, 0, 0, 0, 0, 1], # start from one flowering plant
  t_max = 20
)

mpm.plot.trajectory(
  traj,
  show_classes = [2, 3, 4, 5], # do not show seed classes
  second_order = True,
  rescale = True,
  show_period = True
)
