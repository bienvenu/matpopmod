import matpopmod as mpm

dipsacus = mpm.examples.dipsacus_sylvestris

trajs = dipsacus.stochastic_trajectories(
  [0, 0, 0, 0, 0, 1], # start from one flowering plant
  t_max = 15,
  reps = 1000
)

mpm.plot.multiple_trajectories(trajs, rescale=True)
