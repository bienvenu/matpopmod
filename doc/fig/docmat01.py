import matpopmod as mpm
dipsacus = mpm.examples.dipsacus_sylvestris
mpm.plot.matrices(
  matrices = [dipsacus.elasticities, dipsacus.sensitivities], 
  class_labels = dipsacus.metadata["Classes"],
  matrix_names = ["elasticities", "sensitivities"]
)
