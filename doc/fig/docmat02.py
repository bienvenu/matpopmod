import matpopmod as mpm
dipsacus = mpm.examples.dipsacus_sylvestris
mpm.plot.matrices(
  matrices = dipsacus.elasticities,
  class_labels = dipsacus.metadata["Classes"],
  plot_type = "heatmap"
)
