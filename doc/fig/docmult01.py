import matpopmod as mpm

# Set the PRNG seed, to ensure we get the same trajectories
# for all figures when compiling the documentation
mpm.set_rng_seed(0)

dp = mpm.examples.dipsacus_sylvestris
n0 = [0, 0, 0, 0, 0, 1]
trajs = dp.stochastic_trajectories(n0, t_max=15, reps=1000)
mpm.plot.multiple_trajectories(trajs, rescale = True)
