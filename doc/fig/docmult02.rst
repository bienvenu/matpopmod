.. title:: plot.multiple_trajectories 02

.. h2:: noid Second order, log scale

.. literalinclude:: docmult02.py

.. plot:: fig/docmult02.py
