.. title:: plot.multiple_trajectories 03

.. h2:: noid Centered, rescaled and log scale 

.. literalinclude:: docmult03.py

.. plot:: fig/docmult03.py
