.. title:: plot.trajectory 01

.. h2:: noid Population size, linear scale.

.. literalinclude:: doctraj01.py

.. plot:: fig/doctraj01.py
