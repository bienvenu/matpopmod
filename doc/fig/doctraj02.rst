.. title:: plot.trajectory 02

.. h2:: noid Population size, second order, log scale.

.. literalinclude:: doctraj02.py

.. plot:: fig/doctraj02.py
