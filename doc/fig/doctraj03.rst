.. title:: plot.trajectory 03

.. h2:: noid Class abundances, second order, rescaled. 

.. literalinclude:: doctraj03.py

.. plot:: fig/doctraj03.py
