import matpopmod as mpm
dipsacus = mpm.examples.dipsacus_sylvestris
traj = dipsacus.trajectory([0, 0, 0, 0, 0, 1], 20)
mpm.plot.trajectory(
    traj,
    show_classes = True,
    log = True
)
