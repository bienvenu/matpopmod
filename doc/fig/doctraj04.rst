.. title:: plot.trajectory 04

.. h2:: noid Class abundances, log scale.  

.. literalinclude:: doctraj04.py

.. plot:: fig/doctraj04.py
