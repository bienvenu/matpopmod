import matpopmod as mpm
dipsacus = mpm.examples.dipsacus_sylvestris
traj = dipsacus.trajectory([0, 0, 0, 0, 0, 1], 20)
ax = mpm.plot.trajectory(
    traj,
    show_classes = True,
    stacked = True,
    show_legend = "upper left",
)
# Manually setting the plotting window
ax.set(xlim=(0, 6), ylim=(0, 10000))
