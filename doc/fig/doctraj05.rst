.. title:: plot.trajectory 05

.. h2:: noid Class abundances, stacked, custom window. 

.. literalinclude:: doctraj05.py

.. plot:: fig/doctraj05.py
