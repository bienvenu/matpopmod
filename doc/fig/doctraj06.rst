.. title:: plot.trajectory 06

.. h2:: noid Class abundances, stacked, rescaled.

.. literalinclude:: doctraj06.py

.. plot:: fig/doctraj06.py
