MatPopMod
=========

Matpopmod is a state-of-the-art Python library for the study of matrix
population models.  It can be used to:

.. rst-class:: spaced

- Compute a wide variety of demographic descriptors. This includes all
  of the classic statistics (reproductive values, net reproductive
  rate, generation time, etc) as well as recently introduced ones (e.g,
  measures of kinship and relatedness).

- Study and plot trajectories, both deterministic from the matrix population
  model and stochastic from individual-based simulations.

- Access the COMPADRE / COMADRE databases which, together, contain more than
  12000 ready-to-use projection matrices.

- Perform advanced operations such as redefining the classes of a model,
  sampling random models with prescribed descriptors, etc.


.. rst-class:: image-gallery 

+-----------------------+-----------------------+-----------------------+
|                       |                       |                       |
|  .. a:: fig/01.html   |  .. a:: fig/02.html   |  .. a:: fig/03.html   |
|                       |                       |                       |
|  .. rst-class:: glry  |  .. rst-class:: glry  |  .. rst-class:: glry  |
|                       |                       |                       |
|  .. plot:: fig/01.py  |  .. plot:: fig/02.py  |  .. plot:: fig/03.py  |
|                       |                       |                       |
|  .. a-close::         |  .. a-close::         |  .. a-close::         |
|                       |                       |                       |
|  Life cycle graph     |  Eigenvalues          |  Deterministic        |
|                       |                       |  trajectory           |
|                       |                       |                       |
+-----------------------+-----------------------+-----------------------+
|                       |                       |                       |
|  .. a:: fig/04.html   |  .. a:: fig/05.html   |  .. a:: fig/06.html   |
|                       |                       |                       |
|  .. rst-class:: glry  |  .. rst-class:: glry  |  .. rst-class:: glry  |
|                       |                       |                       |
|  .. plot:: fig/04.py  |  .. plot:: fig/05.py  |  .. plot:: fig/06.py  |
|                       |                       |                       |
|  .. a-close::         |  .. a-close::         |  .. a-close::         |
|                       |                       |                       |
|  Second-order         |  Stochastic           |  Individual-based     |
|  oscillations         |  trajectories         |  simulations          |
|                       |                       |  *(v0.2.0)*           |
+-----------------------+-----------------------+-----------------------+


Matpopmod focuses on ease of use and reliability: 
its implementation is based on rigorous mathematics and thoroughly tested,
and its interface ensures that the user knows exactly what they are computing
without having to worry about technical details.

Each functionality is carefully documented with precise references to
the literature and examples of use.

Installation
------------

Matpopmod requires Python ≥3.6 with NumPy ≥1.10 and Matplotlib.
On Windows, we recommend using the `Anaconda distribution
<https://www.anaconda.com/products/individual>`_. 

Once you have a working installation of Python, you can install matpopmod by
running the following command in your terminal (or in Anaconda Prompt on
Windows)::

  python3 -m pip install matpopmod 

This will take care of installing suitable versions of NumPy and Matplotlib.
In case of problems, see the `Python tutorial on package installation
<https://packaging.python.org/tutorials/installing-packages/>`_.

To upgrade an existing version of matpopmod to the latest release, use::

  python3 -m pip install --upgrade matpopmod

If you want to browse the source code or install the current development
version, visit `our GitLab repository <https://gitlab.com/bienvenu/matpopmod>`_.


Getting started
---------------

After you have installed matpopmod, we suggest reading our
`quickstart guide <quickstart.html>`_. This will give you an overview
of the main functionalities of the library.

Once you have done this and want to know more about a specific topic, you
can read the documentation of the corresponding module.
The complete list of modules, along with a brief description of
what each of them contains, can be found `here <api/modules.html>`_.

In case of problem or question, you can check our
`FAQ <faq.html>`_. If that does not help, do not hesitate to contact us,
either through `GitLab <https://gitlab.com/bienvenu/matpopmod>`_ or
by email using the addresses below.


Authors
-------

+------------------+------------------+
|                  |                  |
|   |contact FB|   |   |contact GD|   |
|                  |                  |
+------------------+------------------+


.. |contact FB| replace::

  François Bienvenu |orcid FB| |br| |newline|
  ``francois.bienvenu`` :math:`\texttt{@}` ``normalesup.org`` |br| |newline|
  `Personal webpage <http://www.normalesup.org/~bienvenu/>`__

.. |contact GD| replace::

  Guilhem Doulcier |orcid GD| |br| |newline|
  ``guilhem.doulcier`` :math:`\texttt{@}` ``normalesup.org`` |br| |newline|
  `Personal webpage <http://www.normalesup.org/~doulcier/>`__

.. |orcid FB| image:: _static/orcid.png
   :target: https://orcid.org/0000-0002-5396-1193
   :width: 16px

.. |orcid GD| image:: _static/orcid.png
   :target: https://orcid.org/0000-0003-3720-9089
   :width: 16px


Acknowledgements
----------------

Matpopmod was written while F. Bienvenu was funded by a Transilvania Fellowship
from the `Transilvania University of Brașov <https://www.unitbv.ro/en/>`_. 


.. |br| raw:: html

  <br/>

.. |newline| raw:: latex 

  \newline

.. |html vspace| raw:: html

  <p></p>


