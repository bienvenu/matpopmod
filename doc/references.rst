.. only:: html

  References
  ==========


.. [BALM17] Bienvenu F, Akçay E, Legendre S. and McCandlish D.M. (2017). The genealogical decomposition of a matrix population model with applications to the aggregation of stages. *Theoretical Population Biology*, 115:69--80.
.. [Bern41] Bernardelli H. (1941). Population Waves. *Journal of the Burma Research Society*, 31(1):3--18.
.. [Bien19] Bienvenu F. (2019). The equivocal mean age of parents in a cohort. *The American Naturalist*, 194(2):276--284.
.. [BiLe15] Bienvenu F. and Legendre S. (2015). A new approach to the generation time in matrix population models. *The American Naturalist*, 185(6):834--843.
.. [BrCa93] Brault S. and Caswell H. (1993). Pod-specific demography of killer whales (*Orcinus orca*). *Ecology*, 74(5):1444--1454.
.. [Casw00] Caswell H. (2000). *Matrix Population Models: Construction, Analysis, and Interpretation*. Sinauer Associates.
.. [Casw78] Caswell H. (1978). A general formula for the sensitivity of population growth rate to changes in life history parameters. *Theoretical Population Biology*, 14(2):215--230.
.. [CaWe78] Caswell H. and Werner P.A. (1978). Transient behavior and life history analysis of teasel (*Dipsacus sylvestris* Huds.). *Ecology*, 59(1):53--66.
.. [CBRR21] Coste C.F.D, Bienvenu F, Ronget V, Ramirez-Loza J.-P, Cubaynes S. and Pavard S. (2021). The kinship matrix: inferring the kinship structure of a population from its demography. *Ecology Letters*, 24(12):2750--2762.
.. [CoEl92] Cochran M. and Ellner S. (1992). Simple methods for calculating age-based life history parameters for stage-structured populations. *Ecological Monographs*, 62(3):345--364.
.. [Cohe79] Cohen J.E. (1979). The cumulative distance from an observed to a stable age structure. *SIAM Journal on Applied Mathematics*, 36(1):169--175.
.. [CuZh94] Cushing J.M. and Zhou Y. (1994). The net reproductive value and stability in matrix population models. *Natural Resource Modeling*, 8(4):297--333.
.. [Deme74] Demetrius L. (1974). Demographic parameters and natural selection. *Proceedings of the National Academy of Sciences*, 71(12):4645--4647.
.. [Elln18] Ellner S. (2018). Generation time in structured populations. *The American Naturalist*, 192(1):105--110.
.. [Gant59] Gantmacher F. (1959). *The Theory of Matrices*. AMS Chelsea publishing.
.. [Hogb06] Hogben L. (2006). *Handbook of Linear Algebra*. CRC press.
.. [HoJo13] Horn R.A. and Johnson C.R. (2012). *Matrix analysis*. Cambridge University Press.
.. [Hool00] Hooley D.E. (2000). Collapsed matrices with (almost) the same eigenstuff. *The College Mathematics Journal*, 31(4):297--299.
.. [HSES15] Henschke N, Smith J.A, Everett J.D. and Suthers I.M. (2015). Population drivers of a Thalia democratica swarm: insights from population modelling. *Journal of Plankton Research*, 37(5):1074--1087.
.. [KeCa05] Keyfitz N. and Caswell H. (2005). *Applied mathematical demography*. Springer.
.. [KeFl71] Keyfitz N. and Flieger W. (1971). *Population: facts and methods of demography*. W. H. Freeman.
.. [Keyf71] Keyfitz N. (1971). On the momentum of population growth. *Demography*, 8(1):71--80.
.. [Keyf77] Keyfitz N. (1977). What difference would it make if cancer were eradicated? An examination of the Taeuber paradox. *Demography*, 14(4):411--418.
.. [KeSn76] Kemeny J.G. and Snell J.L. (1976). *Finite Markov Chains*. Springer--Verlag.
.. [KiNe19] Kirkland S.J. and Neumann M. (2019). *Group Inverses of M-matrices and their Applications*. Chapman and Hall/CRC.
.. [LeCl95] Legendre S. and Clobert J. (1995). ULM, a software for conservation and evolutionary biologists. *Journal of Applied Statistics*, 22:817--834.
.. [Meye00] Meyer C.D. (2000). *Matrix Analysis and Applied Linear Algebra*. SIAM.
.. [PiMS84] Pinero D, Martinez-Ramos M. and Sarukhan J. (1984). A population model of *Astrocaryum mexicanum* and a sensitivity analysis of its finite rate of increase. Journal of Ecology, 72(3):977--991.
.. [Roth81] Rothblum U.G. (1981). Expansions of Sums of Matrix Powers. *SIAM Review*, 23(2):143--164.
.. [Salg15] Salguero-Gómez R. et al. (2015). The COMPADRE Plant Matrix Database: an open online repository for plant demography. *Journal of Ecology*, 103(1):202--218. 
.. [Salg16] Salguero-Gómez R. et al. (2016). COMADRE: a global data base of animal demography. *Journal of Animal Ecology*. 85(2):371--384. 
.. [Sene06] Seneta E. (2006). *Non-negative Matrices and Markov Chains*. Springer.
.. [SmKe77] Smith D. and Keyfitz N. (1977). *Mathematical Demography: Selected Papers*. Springer--Verlag.
.. [StTC14] Steiner U.K, Tuljapurkar S. and Coulson T. (2014). Generation time, net reproductive rate, and growth in stage-age-structured populations. *The American Naturalist*, 183(6):771--783.
.. [Tarj72] Tarjan R.E. (1972). Depth-first search and linear graph algorithms. *SIAM Journal on Computing*, 1(2):146--160.
.. [Tulj82] Tuljapurkar S. (1982). Why use population entropy? It determines the rate of convergence. *Journal of Mathematical Biology*, 13(3):325--337.
.. [Vahr76] Vahrenkamp R. (1976). Derivatives of the dominant root. *Applied Mathematics and Computation*, 2(1):29--39.
.. [Varg00] Varga R.S. (2000). *Matrix Iterative Analysis*. Springer.


.. toctree::


