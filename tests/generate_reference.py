"""
THIS FILE SHOULD NEVER BE EXECUTED.

It was meant to be used a executed a single time to assist the
production of the reference files for `test_references.py`, but
since then the references have been checked and edited manually.
"""

import os
from datetime import datetime
import numpy as np
import matpopmod as mpm
from test_reference import FIELDS, FIELDS_n0, FIELDS_t
from collections import namedtuple

PATH = "reference_values"
input_output = namedtuple("in_out","input output")

np.set_printoptions(floatmode="maxprec", precision=8, linewidth=9999)

# Create a list of pairs with the (field name, list of input),
# the list of input is either NONE (non parametric attributes) or
# and a list function of the MPM. 
ALL_FIELDS = (list(zip(FIELDS, [None]*len(FIELDS)))
              +list(zip(FIELDS_t, [(lambda m: 0,lambda m: 1)]*len(FIELDS_t)))
              +list(zip(FIELDS_n0, [(lambda m: np.eye(m.dim)[0], lambda m: m.w)]*len(FIELDS_n0)))
              )
        
def get_code(model, field, input_functions=None):
    error = None 
    if input_functions is None:
        try:
            value = getattr(model, field)
        except Exception as error:
            code = '"{}" # UNCHECKED\n'.format(error.__repr__())
        else:
            if isinstance(value, np.ndarray):
                code = "( # UNCHECKED\n{})\n".format(value.__repr__())
            elif isinstance(value, tuple):
                code = "( # UNCHECKED\n"
                for x in value:
                  code += "{},\n".format(x.__repr__())
                code += ")\n"
            else:
                code = "{} # UNCHECKED\n".format(value.__repr__())
    else:
        code = "(# Pairs of input/output:\n"
        for i in input_functions:
            code += "  input_output( # UNCHECKED"
            try:
                arg = i(model)
            except Exception as error:
                code += '\n    input = "{}",'.format(error.__repr__())
            else:
                np.set_printoptions(floatmode="maxprec", precision=99, linewidth=9999)
                code +="\n    input = {},".format(arg.__repr__())
                np.set_printoptions(floatmode="maxprec", precision=8, linewidth=9999)
            try:
                value = getattr(model, field)(i(model))
            except Exception as error:
                code += '\n    output = "{}",'.format(error.__repr__())
            else:
                code +="\n    output = {},".format(value.__repr__())
            code += "\n  ),\n"
        code += ")\n"
    return code


__all__ = []
if __name__ == "__main__":
    os.makedirs(PATH, exist_ok=True)
    for m in mpm.examples.all_models:
        fileout = os.path.join("reference_values", m.metadata['ModelName']+'.py')
        data = []
        data.append('"""\nReference values for model {}.'.format(m.metadata['ModelName']))
        data.append('Values marked as "# UNCHECKED" have not yet been checked by a human.')
        data.append('Otherwise, a comment indicates who checked the value and how.\n"""')
        data.append("")
        data.append('from collections import namedtuple')
        data.append("import numpy as np")
        data.append("nan = np.nan")
        data.append("inf = np.inf")
        data.append("array  = np.array")
        data.append('input_output = namedtuple("input_output","input output")')
        data.append(" ")
        
        for f, inputs in ALL_FIELDS:
            data.append("{} = {}".format(f, get_code(m, f, inputs)))
        with open(fileout, 'w') as file:
            file.write("\n".join(data))
        __all__.append(m.metadata['ModelName'])
    
    with open(os.path.join("reference_values", "__init__.py"),'w') as file:
        file.write("__all__ = {}".format(__all__))
