"""
Reference values for model bernardelli_beetle.
Values marked as "# UNCHECKED" have not yet been checked by a human.
Otherwise, a comment indicates who checked the value and how.
"""

from collections import namedtuple
import numpy as np
nan = np.nan
inf = np.inf
array  = np.array
input_output = namedtuple("input_output","input output")
 
irreducible = True # FB (analytical)

irreducible_components = ( # FB (analytical)
[array([[0.        , 0.        , 6.        ],
       [0.5       , 0.        , 0.        ],
       [0.        , 0.33333333, 0.        ]])],
[[0, 1, 2]],
)

normal_form = ( # FB (analytical)
array([[0.        , 0.        , 6.        ],
       [0.5       , 0.        , 0.        ],
       [0.        , 0.33333333, 0.        ]]),
array([0, 1, 2]),
)

periods = [3, 3, 3] # FB (analytical)

index_of_imprimitivity = 3 # FB (analytical)

aperiodic = False # FB (analytical)

primitive = False # FB (analytical)

quasi_irreducible = True # FB (analytical)

quasi_primitive = False # FB (analytical)

eigenvalues = ( # FB (analytical)
1.0,
(-0.5000000000000004+0.8660254037844385j),
(-0.5000000000000004-0.8660254037844385j),
)

right_eigenvectors = ( # UNCHECKED
array([0.6, 0.3, 0.1]),
array([ 0.6 +0.j        , -0.15-0.25980762j, -0.05+0.08660254j]),
array([ 0.6 +0.j        , -0.15+0.25980762j, -0.05-0.08660254j]),
)

left_eigenvectors = ( # UNCHECKED
array([0.11111111, 0.22222222, 0.66666667]),
array([ 0.05555556-0.09622504j,  0.11111111+0.19245009j, -0.66666667+0.j        ]),
array([ 0.05555556+0.09622504j,  0.11111111-0.19245009j, -0.66666667+0.j        ]),
)

lmbd = 1.0 # FB (analytical)

w = ( # FB (not primitive)
array([nan, nan, nan]))

v = ( # FB (not primitive)
array([nan, nan, nan]))

damping_ratio = inf # FB (analytical)

second_order_period = nan # FB (analytical)

sensitivities = ( # UNCHECKED
array([[0.33333333, 0.16666667, 0.05555556],
       [0.66666667, 0.33333333, 0.11111111],
       [2.        , 1.        , 0.33333333]]))

elasticities = ( # FB (analytical)
array([[0.        , 0.        , 0.33333333],
       [0.33333333, 0.        , 0.        ],
       [0.        , 0.33333333, 0.        ]]))

P = ( # FB (not primitive)
array([[nan, nan, nan],
       [nan, nan, nan],
       [nan, nan, nan]]))

Ps = ( # FB (not primitive)
array([[nan, nan, nan],
       [nan, nan, nan],
       [nan, nan, nan]]))

Pf = ( # FB (not primitive)
array([[nan, nan, nan],
       [nan, nan, nan],
       [nan, nan, nan]]))

pi = ( # FB (not primitive)
array([nan, nan, nan]))

entropy_rate = nan # FB (not primitive)

mixed_transitions = False # FB (analytical)

fundamental_matrix = ( # FB (analytical)
array([[1.        , 0.        , 0.        ],
       [0.5       , 1.        , 0.        ],
       [0.16666667, 0.33333333, 1.        ]]))

G = ( # FB (analytical)
array([[1., 2., 6.],
       [0., 0., 0.],
       [0., 0., 0.]]))

fundamental_matrix_Ps = ( # FB (not primitive)
array([[nan, nan, nan],
       [nan, nan, nan],
       [nan, nan, nan]]))

newborn_classes = ( # FB (analytical)
array([1., 0., 0.]))

unique_newborn_class = True # FB (analytical)

reproductive_classes = ( # FB (analytical)
array([0., 0., 1.]))

postreproductive_classes = ( # FB (analytical)
array([0., 0., 0.]))

proportion_newborns = ( # FB (analytical)
array([1., 0., 0.]))

nu = ( # FB (analytical)
array([1., 0., 0.]))

class_of_birth = ( # FB (analytical)
array([[1., 1., 1.],
       [0., 0., 0.],
       [0., 0., 0.]]))

class_of_death = "NotImplementedError()" # UNCHECKED

leslie = True # FB (analytical)

usher = True # FB (analytical)

relabeled_leslie = "NotImplementedError()" # UNCHECKED

relabeled_usher = "NotImplementedError()" # UNCHECKED

kemeny_constant = "NotImplementedError()" # UNCHECKED

hitting_times = "NotImplementedError()" # UNCHECKED

R0 = 1.0 # FB (analytical)

cohort_R0 = 1.0 # FB (analytical)

total_reproductive_output = ( #
array([[nan, nan, nan],
       [nan, nan, nan],
       [nan, nan, nan]]))

T_a = nan # FB (not primitive; maybe we could extend the definition in the future)

T_G = 3.0 # UNCHECKED

T_R0 = nan # FB (not primitive)

mu1 = nan # FB (not primitive)

proba_repro = ( # FB (comparison with IBMs)
array([0.16625354, 0.33250708, 0.99752125]))

life_expectancy_repro = 3.0 # FB (analytical)

remaining_life_expectancy_repro = ( # FB (analytical)
array([ 3.,  2., 1.]))

w_G = ( # FB (analytical)
array([1., 0., 0.]))

v_G = ( # UNCHECKED
array([1., 2., 6.]))

proba_maturity = "NotImplementedError()" # UNCHECKED

conditional_life_expectancy = "NotImplementedError()" # UNCHECKED

remaining_life_expectancy = ( # FB (analytical)
array([1.66666667, 1.33333333, 1.        ]))

life_expectancy = 1.6666666666666667 # FB (analytical)

variance_remaining_lifespan = "NotImplementedError()" # UNCHECKED

variance_lifespan = "NotImplementedError()" # UNCHECKED

mean_age_class = ( # FB (analytical)
array([1., 2., 3.]))

mean_age_population = nan # FB (not primitive)

variance_age_class = "NotImplementedError()" # UNCHECKED

variance_age_population = "NotImplementedError()" # UNCHECKED

mean_age_maturity = "NotImplementedError()" # UNCHECKED

mean_age_first_repro = "NotImplementedError()" # UNCHECKED

population_entropy = nan # UNCHECKED

birth_entropy = "NotImplementedError()" # UNCHECKED

genealogical_entropy = "NotImplementedError()" # UNCHECKED

lifetable_entropy = 0.38712010109078904 # UNCHECKED

fertility_excess = 1.0 # FB (analytical)

survival_excess = 1.0 # FB (analytical)

theta = "NotImplementedError()" # UNCHECKED

survivorship = (# Pairs of input/output:
  input_output( # FB (analytical)
    input = 0,
    output = 1.0,
  ),
  input_output( # FB (analytical)
    input = 1,
    output = 0.5,
  ),
)

class_survivorship = (# Pairs of input/output:
  input_output( # FB (analytical)
    input = 0,
    output = array([1., 1., 1.]),
  ),
  input_output( # FB (analytical)
    input = 1,
    output = array([0.5       , 0.33333333, 0.        ]),
  ),
)

age_specific_fertility = (# Pairs of input/output:
  input_output( # FB (analytical)
    input = 0,
    output = array([[0., 0., 6.],
       [0., 0., 0.],
       [0., 0., 0.]]),
  ),
  input_output( # UNCHECKED
    input = 1,
    output = array([[ 0.,  6., nan],
       [ 0.,  0., nan],
       [ 0.,  0., nan]]),
  ),
)

keyfitz_delta = (# Pairs of input/output:
  input_output( # FB (not primitive)
    input = array([1., 0., 0.]),
    output = nan,
  ),
  input_output( # FB (not primitive)
    input = array([nan, nan, nan]),
    output = "MissingEntries('Array n0 contains NaN entries: (0,), (1,), (2,).')",
  ),
)

cohen_D1 = (# Pairs of input/output:
  input_output( # FB (not primitive)
    input = array([1., 0., 0.]),
    output = nan,
  ),
  input_output( # FB (not primitive) 
    input = array([nan, nan, nan]),
    output = "MissingEntries('Array n0 contains NaN entries: (0,), (1,), (2,).')",
  ),
)

population_momentum = (# Pairs of input/output:
  input_output( # FB (not primitive)
    input = array([1., 0., 0.]),
    output = nan,
  ),
  input_output( # FB (not primitive)
    input = array([nan, nan, nan]),
    output = nan,
  ),
)
