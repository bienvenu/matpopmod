"""
Reference values for model orcinus_orca.
Values marked as "# UNCHECKED" have not yet been checked by a human.
Otherwise, a comment indicates who checked the value and how.
"""

from collections import namedtuple
import numpy as np
nan = np.nan
inf = np.inf
array  = np.array
input_output = namedtuple("input_output","input output")
 
irreducible = False # FB (analytical)

irreducible_components = ( # FB (analytical)
[array([[0.    , 0.0043, 0.1132],
       [0.9775, 0.9111, 0.    ],
       [0.    , 0.0736, 0.9534]]), array([[0.9804]])],
[[0, 1, 2], [3]],
)

normal_form = ( # FB (analytical)
array([[0.    , 0.0043, 0.1132, 0.    ],
       [0.9775, 0.9111, 0.    , 0.    ],
       [0.    , 0.0736, 0.9534, 0.    ],
       [0.    , 0.    , 0.0452, 0.9804]]),
array([0, 1, 2, 3]),
)

periods = [1, 1, 1, 1] # FB (analytical)

index_of_imprimitivity = 1 # FB (analytical)

aperiodic = True # FB (analytical)

primitive = False # FB (analytical)

quasi_irreducible = True # FB (analytical)

quasi_primitive = True # FB (analytical)

eigenvalues = ( # FB (ULM)
1.0254413255303465,
0.9804,
0.8342229761377999,
0.004835698331853053,
)

right_eigenvectors = ( # UNCHECKED
array([0.03697187, 0.31607121, 0.32290968, 0.32404724]),
array([0., 0., 0., 1.]),
array([ 0.04167421, -0.52989222,  0.32724485, -0.10118873]),
array([ 0.46164405, -0.49793097,  0.03863493, -0.00179004]),
)

left_eigenvectors = ( # UNCHECKED
array([ 0.27621523,  0.28976216,  0.43402261, -0.        ]),
array([ 0.20558696,  0.20619688,  0.18213886, -0.40607731]),
array([ 0.35672593,  0.30443884, -0.33883524, -0.        ]),
array([ 0.88945399,  0.00440013, -0.10614588,  0.        ]),
)

lmbd = 1.0254413255303465 # FB (ULM) 

w = ( # FB (ULM; [KeCa05])
array([0.03697187, 0.31607121, 0.32290968, 0.32404724]))

v = ( # FB (ULM; [KeCa05])
array([ 1.14163164,  1.19762278,  1.79386902, -0.        ]))

damping_ratio = 1.0459417845066774 # FB (ULM)

second_order_period = nan # FB (ULM)

sensitivities = ( # FB (ULM)
array([[0.04220825, 0.3608369 , 0.3686439 , 0.36994259],
       [0.04427835, 0.37853408, 0.38672398, 0.38808636],
       [0.06632269, 0.56699035, 0.57925766, 0.58129831],
       [0.        , 0.        , 0.        , 0.        ]]))

elasticities = ( # FB (ULM)
array([[0.        , 0.0015131 , 0.04069515, 0.        ],
       [0.04220825, 0.33632583, 0.        , 0.        ],
       [0.        , 0.04069515, 0.53856251, 0.        ],
       [0.        , 0.        , 0.        , 0.        ]]))

P = ( # FB ([BiLe15])
array([[0.        , 0.03584852, 0.96415148, 0.        ],
       [0.1115045 , 0.8884955 , 0.        , 0.        ],
       [0.        , 0.07025397, 0.92974603, 0.        ],
       [0.        , 0.        , 0.04392384, 0.95607616]]))

Ps = ( # FB ([BiLe15])
array([[0.        , 0.        , 0.        , 0.        ],
       [0.1115045 , 0.8884955 , 0.        , 0.        ],
       [0.        , 0.07025397, 0.92974603, 0.        ],
       [0.        , 0.        , 0.04392384, 0.95607616]]))

Pf = ( # FB ([BiLe15])
array([[0.        , 0.03584852, 0.96415148, 0.        ],
       [0.        , 0.        , 0.        , 0.        ],
       [0.        , 0.        , 0.        , 0.        ],
       [0.        , 0.        , 0.        , 0.        ]]))

pi = ( # UNCHECKED
array([ 0.04220825,  0.37853408,  0.57925766, -0.        ]))

entropy_rate = 0.4128685886722462 # FB (ULM, noting difference of base for the log)

mixed_transitions = False # FB (analytical)

fundamental_matrix = ( # FB ([BiLe15]; [KeCa05]) 
array([[ 1.        ,  0.        ,  0.        ,  0.        ],
       [10.99550056, 11.24859393,  0.        ,  0.        ],
       [17.36628415, 17.76601959, 21.45922747,  0.        ],
       [40.04877773, 40.97061661, 49.4876062 , 51.02040816]]))

G = ( # UNCHECKED
array([[2.01314402, 2.05948237, 2.42918455, 0.        ],
       [0.        , 0.        , 0.        , 0.        ],
       [0.        , 0.        , 0.        , 0.        ],
       [0.        , 0.        , 0.        , 0.        ]]))

fundamental_matrix_Ps = ( # UNCHECKED
array([[ 1.        ,  0.        ,  0.        ,  0.        ],
       [ 1.        ,  8.96824766,  0.        ,  0.        ],
       [ 1.        ,  8.96824766, 14.23407076,  0.        ],
       [ 1.        ,  8.96824766, 14.23407076, 22.76667734]]))

newborn_classes = ( # FB (analytical)
array([1., 0., 0., 0.]))

unique_newborn_class = True # FB (analytical)

reproductive_classes = ( # FB (analytical)
array([0., 1., 1., 0.]))

postreproductive_classes = ( # FB (analytical)
array([0., 0., 0., 1.]))

proportion_newborns = ( # FB (analytical)
array([1., 0., 0., 0.]))

nu = ( # FB (analytical)
array([1., 0., 0., 0.]))

class_of_birth = ( # FB (analytical)
array([[1., 1., 1., 1.],
       [0., 0., 0., 0.],
       [0., 0., 0., 0.],
       [0., 0., 0., 0.]]))

class_of_death = "NotImplementedError()" # UNCHECKED

leslie = False # FB (analytical)

usher = True # FB (analytical)

relabeled_leslie = "NotImplementedError()" # UNCHECKED

relabeled_usher = "NotImplementedError()" # UNCHECKED

kemeny_constant = "NotImplementedError()" # UNCHECKED

hitting_times = "NotImplementedError()" # UNCHECKED

R0 = 2.0131440182101716 # FB (ULM)

cohort_R0 = 2.0131440182101716 # FB (analytical: equal to R0 because 1 newborn class)

total_reproductive_output = ( # UNCHECKED
array([[2.01314402, 2.09374584, 3.96584482, 1.64986028],
       [0.        , 0.        , 0.        , 0.        ],
       [0.        , 0.        , 0.        , 0.        ],
       [0.        , 0.        , 0.        , 0.        ]]))

T_a = 23.69204811243964 # FB (ULM)

T_G = 33.203830488848176 # UNCHECKED

T_R0 = 27.850790932634087 # FB (ULM)

mu1 = 33.20383048884817 # FB ([BiLe15]) 

proba_repro = ( # FB (comparison with IBMs)
array([0.60096267, 0.61479557, 0.72004594, 0.        ]))

life_expectancy_repro = 87.87103293619145 # FB (comparison with IBMs)

remaining_life_expectancy_repro = ( # FB (comparison with IBMs)
array([87.87103294, 86.87103294, 76.67447748,         nan]))

w_G = ( # UNCHECKED
array([1., 0., 0., 0.]))

v_G = ( # UNCHECKED
array([1.        , 1.0230179 , 1.20666208, 0.        ]))

proba_maturity = "NotImplementedError()" # UNCHECKED

conditional_life_expectancy = "NotImplementedError()" # UNCHECKED

remaining_life_expectancy = ( # UNCHECKED
array([69.41056245, 69.98523012, 70.94683367, 51.02040816]))

life_expectancy = 69.41056244644568 # UNCHECKED

variance_remaining_lifespan = "NotImplementedError()" # UNCHECKED

variance_lifespan = "NotImplementedError()" # UNCHECKED

mean_age_class = ( # UNCHECKED
array([ 1.        ,  9.96824766, 24.20231843, 46.96899577]))

mean_age_population = 26.222984422352624 # UNCHECKED

variance_age_class = "NotImplementedError()" # UNCHECKED

variance_age_population = "NotImplementedError()" # UNCHECKED

mean_age_maturity = "NotImplementedError()" # UNCHECKED

mean_age_first_repro = "NotImplementedError()" # UNCHECKED

population_entropy = 9.781702466937908 # UNCHECKED

birth_entropy = "NotImplementedError()" # UNCHECKED

genealogical_entropy = "NotImplementedError()" # UNCHECKED

lifetable_entropy = 0.8276633700123288 # UNCHECKED

fertility_excess = 2.013144018186722 # FB (manually)

survival_excess = 1.0265844900277443 # FB (manually)

theta = "NotImplementedError()" # UNCHECKED

survivorship = (# Pairs of input/output:
  input_output( # FB (analytical)
    input = 0,
    output = 1.0,
  ),
  input_output( # FB (analytical)
    input = 1,
    output = 0.9775,
  ),
)

class_survivorship = (# Pairs of input/output:
  input_output( # FB (analytical)
    input = 0,
    output = array([1., 1., 1., 1.]),
  ),
  input_output( # FB (analytical)
    input = 1,
    output = array([0.9775, 0.9847, 0.9986, 0.9804]),
  ),
)

age_specific_fertility = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = 0,
    output = array([[0.    , 0.0043, 0.1132, 0.    ],
       [0.    , 0.    , 0.    , 0.    ],
       [0.    , 0.    , 0.    , 0.    ],
       [0.    , 0.    , 0.    , 0.    ]]),
  ),
  input_output( # UNCHECKED
    input = 1,
    output = array([[0.0043    , 0.01243958, 0.10807619, 0.        ],
       [0.        , 0.        , 0.        , 0.        ],
       [0.        , 0.        , 0.        , 0.        ],
       [0.        , 0.        , 0.        , 0.        ]]),
  ),
)

keyfitz_delta = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = array([1., 0., 0., 0.]),
    output = 0.9630281317141139,
  ),
  input_output( # FB (analytical)
    input = array([0.036971868285886084, 0.3160712111900605  , 0.32290967680472743 , 0.324047243719326   ]),
    output = 0.0,
  ),
)

cohen_D1 = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = array([1., 0., 0., 0.]),
    output = 16.67440014813036,
  ),
  input_output( # FB (analytical, should be 0)
    input = array([0.036971868285886084, 0.3160712111900605  , 0.32290967680472743 , 0.324047243719326   ]),
    output = 7.077671781985373e-16,
  ),
)

population_momentum = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = array([1., 0., 0., 0.]),
    output = 2.090438404952875,
  ),
  input_output( # UNCHECKED
    input = array([0.036971868285886084, 0.3160712111900605  , 0.32290967680472743 , 0.324047243719326   ]),
    output = 1.5677477748209103,
  ),
)
