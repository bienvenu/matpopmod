"""
Reference values for model passerine_postbreeding.
Values marked as "# UNCHECKED" have not yet been checked by a human.
Otherwise, a comment indicates who checked the value and how.
"""

from collections import namedtuple
import numpy as np
nan = np.nan
inf = np.inf
array  = np.array
input_output = namedtuple("input_output","input output")
 
irreducible = True # UNCHECKED

irreducible_components = ( # UNCHECKED
[array([[0.7  , 1.225, 1.75 ],
       [0.2  , 0.   , 0.   ],
       [0.   , 0.35 , 0.5  ]])],
[[0, 1, 2]],
)

normal_form = ( # UNCHECKED
array([[0.7  , 1.225, 1.75 ],
       [0.2  , 0.   , 0.   ],
       [0.   , 0.35 , 0.5  ]]),
array([0, 1, 2]),
)

periods = [1, 1, 1] # UNCHECKED

index_of_imprimitivity = 1 # UNCHECKED

aperiodic = True # UNCHECKED

primitive = True # UNCHECKED

quasi_irreducible = True # UNCHECKED

quasi_primitive = True # UNCHECKED

eigenvalues = ( # UNCHECKED
1.1049752469181024,
0.09502475308189617,
0.0,
)

right_eigenvectors = ( # UNCHECKED
array([0.77777778, 0.14077741, 0.08144481]),
array([ 0.20309867,  0.42746476, -0.36943657]),
array([-0.        ,  0.58823529, -0.41176471]),
)

left_eigenvectors = ( # UNCHECKED
array([0.16898867, 0.34218114, 0.4888302 ]),
array([ 0.11981604, -0.36242869, -0.51775527]),
array([ 0.125 , -0.4375, -0.4375]),
)

lmbd = 1.1049752469181024 # UNCHECKED

w = ( # UNCHECKED
array([0.77777778, 0.14077741, 0.08144481]))

v = ( # UNCHECKED
array([0.77016183, 1.55948238, 2.22783197]))

damping_ratio = 11.628288536206878 # UNCHECKED

second_order_period = nan # UNCHECKED

sensitivities = ( # UNCHECKED
array([[0.59901475, 0.10842139, 0.06272568],
       [1.21293074, 0.21953989, 0.12701175],
       [1.7327582 , 0.31362842, 0.18144535]]))

elasticities = ( # UNCHECKED
array([[0.37947486, 0.12019835, 0.09934154],
       [0.21953989, 0.        , 0.        ],
       [0.        , 0.09934154, 0.08210381]]))

P = ( # UNCHECKED
array([[0.63349835, 0.20066008, 0.16584156],
       [1.        , 0.        , 0.        ],
       [0.        , 0.54750118, 0.45249882]]))

Ps = ( # UNCHECKED
array([[0.        , 0.        , 0.        ],
       [1.        , 0.        , 0.        ],
       [0.        , 0.54750118, 0.45249882]]))

Pf = ( # UNCHECKED
array([[0.63349835, 0.20066008, 0.16584156],
       [0.        , 0.        , 0.        ],
       [0.        , 0.        , 0.        ]]))

pi = ( # UNCHECKED
array([0.59901475, 0.21953989, 0.18144535]))

entropy_rate = 0.9662055594277512 # UNCHECKED

mixed_transitions = False # UNCHECKED

fundamental_matrix = ( # UNCHECKED
array([[1.  , 0.  , 0.  ],
       [0.2 , 1.  , 0.  ],
       [0.14, 0.7 , 2.  ]]))

G = ( # UNCHECKED
array([[1.19, 2.45, 3.5 ],
       [0.  , 0.  , 0.  ],
       [0.  , 0.  , 0.  ]]))

fundamental_matrix_Ps = ( # UNCHECKED
array([[1.       , 0.       , 0.       ],
       [1.       , 1.       , 0.       ],
       [1.       , 1.       , 1.8264801]]))

newborn_classes = ( # UNCHECKED
array([1., 0., 0.]))

unique_newborn_class = True # UNCHECKED

reproductive_classes = ( # UNCHECKED
array([1., 1., 1.]))

postreproductive_classes = ( # UNCHECKED
array([0., 0., 0.]))

proportion_newborns = ( # UNCHECKED
array([1., 0., 0.]))

nu = ( # UNCHECKED
array([1., 0., 0.]))

class_of_birth = ( # UNCHECKED
array([[1., 1., 1.],
       [0., 0., 0.],
       [0., 0., 0.]]))

class_of_death = "NotImplementedError()" # UNCHECKED

leslie = True # UNCHECKED

usher = True # UNCHECKED

relabeled_leslie = "NotImplementedError()" # UNCHECKED

relabeled_usher = "NotImplementedError()" # UNCHECKED

kemeny_constant = "NotImplementedError()" # UNCHECKED

hitting_times = "NotImplementedError()" # UNCHECKED

R0 = 1.19 # UNCHECKED

cohort_R0 = 1.19 # UNCHECKED

total_reproductive_output = ( # UNCHECKED
array([[1.19      , 3.15      , 6.87134017],
       [0.        , 0.        , 0.        ],
       [0.        , 0.        , 0.        ]]))

T_a = 1.669407961699505 # UNCHECKED

T_G = 1.823529411764706 # UNCHECKED

T_R0 = 1.742618660928268 # UNCHECKED

mu1 = 1.8235294117647058 # UNCHECKED

proba_repro = ( # FB (comparison with IBM)
array([0.58279626, 0.79927415, 0.90484533]))

life_expectancy_repro = 1.5473617665317376 # FB (comparison with IBM)

remaining_life_expectancy_repro = ( # FB (comparison with IBM)
array([1.54736177, 1.86238961, 2.09515467]))

w_G = ( # UNCHECKED
array([1., 0., 0.]))

v_G = ( # UNCHECKED
array([1.        , 2.05882353, 2.94117647]))

proba_maturity = "NotImplementedError()" # UNCHECKED

conditional_life_expectancy = "NotImplementedError()" # UNCHECKED

remaining_life_expectancy = ( # UNCHECKED
array([1.34, 1.7 , 2.  ]))

life_expectancy = 1.3399999999999999 # UNCHECKED

variance_remaining_lifespan = "NotImplementedError()" # UNCHECKED

variance_lifespan = "NotImplementedError()" # UNCHECKED

mean_age_class = ( # UNCHECKED
array([1.       , 2.       , 3.8264801]))

mean_age_population = 1.370979547044335 # UNCHECKED

variance_age_class = "NotImplementedError()" # UNCHECKED

variance_age_population = "NotImplementedError()" # UNCHECKED

mean_age_maturity = "NotImplementedError()" # UNCHECKED

mean_age_first_repro = "NotImplementedError()" # UNCHECKED

population_entropy = 1.6129912535470121 # UNCHECKED

birth_entropy = "NotImplementedError()" # UNCHECKED

genealogical_entropy = "NotImplementedError()" # UNCHECKED

lifetable_entropy = 0.5904661141199093 # UNCHECKED

fertility_excess = 1.190000000060536 # UNCHECKED

survival_excess = 1.31666666676756 # UNCHECKED

theta = "NotImplementedError()" # UNCHECKED

survivorship = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = 0,
    output = 1.0,
  ),
  input_output( # UNCHECKED
    input = 1,
    output = 0.2,
  ),
)

class_survivorship = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = 0,
    output = array([1., 1., 1.]),
  ),
  input_output( # UNCHECKED
    input = 1,
    output = array([0.2 , 0.35, 0.5 ]),
  ),
)

age_specific_fertility = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = 0,
    output = array([[0.7  , 1.225, 1.75 ],
       [0.   , 0.   , 0.   ],
       [0.   , 0.   , 0.   ]]),
  ),
  input_output( # UNCHECKED
    input = 1,
    output = array([[1.225, 1.75 , 1.75 ],
       [0.   , 0.   , 0.   ],
       [0.   , 0.   , 0.   ]]),
  ),
)

keyfitz_delta = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = array([1., 0., 0.]),
    output = 0.22222222222222215,
  ),
  input_output( # UNCHECKED
    input = array([0.7777777777777779 , 0.14077741197317944, 0.08144481024904275]),
    output = 1.3183898417423734e-16,
  ),
)

cohen_D1 = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = array([1., 0., 0.]),
    output = 0.5990809688088603,
  ),
  input_output( # UNCHECKED
    input = array([0.7777777777777779 , 0.14077741197317944, 0.08144481024904275]),
    output = 1.0408340855860843e-15,
  ),
)

population_momentum = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = array([1., 0., 0.]),
    output = 0.7348387096667547,
  ),
  input_output( # UNCHECKED
    input = array([0.7777777777777779 , 0.14077741197317944, 0.08144481024904275]),
    output = 0.9605496995365475,
  ),
)
