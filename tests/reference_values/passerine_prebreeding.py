"""
Reference values for model passerine_prebreeding.
Values marked as "# UNCHECKED" have not yet been checked by a human.
Otherwise, a comment indicates who checked the value and how.
"""

from collections import namedtuple
import numpy as np
nan = np.nan
inf = np.inf
array  = np.array
input_output = namedtuple("input_output","input output")
 
irreducible = True # GD (popdemo 1.3.0)

irreducible_components = ( # UNCHECKED
[array([[0.7 , 0.7 ],
       [0.35, 0.5 ]])],
[[0, 1]],
)

normal_form = ( # UNCHECKED
array([[0.7 , 0.7 ],
       [0.35, 0.5 ]]),
array([0, 1]),
)

periods = [1, 1] # UNCHECKED

index_of_imprimitivity = 1 # UNCHECKED

aperiodic = True # UNCHECKED

primitive = True # GD (popdemo 1.3.0)

quasi_irreducible = True # UNCHECKED

quasi_primitive = True # UNCHECKED

eigenvalues = ( # UNCHECKED
1.1049752469181038,
0.09502475308189612,
)

right_eigenvectors = ( # UNCHECKED
array([0.63349835, 0.36650165]),
array([ 0.53640864, -0.46359136]),
)

left_eigenvectors = ( # UNCHECKED
array([0.46359136, 0.53640864]),
array([ 0.36650165, -0.63349835]),
)

lmbd = 1.1049752469181038 # GD (popdemo 1.3.0)

w = ( # GD (popdemo 1.3.0)
array([0.63349835, 0.36650165]))

v = ( # GD (popdemo 1.3.0)
array([0.94556639, 1.09408853]))

damping_ratio = 11.628288536206899 # GD (popdemo 1.3.0)

second_order_period = nan # UNCHECKED

sensitivities = ( # GD (popdemo 1.3.0)
array([[0.59901475, 0.34655164],
       [0.69310328, 0.40098525]]))

elasticities = ( # GD (popdemo 1.3.0)
array([[0.37947486, 0.21953989],
       [0.21953989, 0.18144535]]))

P = ( # UNCHECKED
array([[0.63349835, 0.36650165],
       [0.54750118, 0.45249882]]))

Ps = ( # UNCHECKED
array([[0.        , 0.        ],
       [0.54750118, 0.45249882]]))

Pf = ( # UNCHECKED
array([[0.63349835, 0.36650165],
       [0.        , 0.        ]]))

pi = ( # UNCHECKED
array([0.59901475, 0.40098525]))

entropy_rate = 0.9662055594277523 # UNCHECKED

mixed_transitions = False # UNCHECKED

fundamental_matrix = ( # UNCHECKED
array([[1. , 0. ],
       [0.7, 2. ]]))

G = ( # UNCHECKED
array([[1.19, 1.4 ],
       [0.  , 0.  ]]))

fundamental_matrix_Ps = ( # UNCHECKED
array([[1.       , 0.       ],
       [1.       , 1.8264801]]))

newborn_classes = ( # UNCHECKED
array([1., 0.]))

unique_newborn_class = True # UNCHECKED

reproductive_classes = ( # UNCHECKED
array([1., 1.]))

postreproductive_classes = ( # UNCHECKED
array([0., 0.]))

proportion_newborns = ( # UNCHECKED
array([1., 0.]))

nu = ( # UNCHECKED
array([1., 0.]))

class_of_birth = ( # UNCHECKED
array([[1., 1.],
       [0., 0.]]))

class_of_death = "NotImplementedError()" # UNCHECKED

leslie = True # UNCHECKED

usher = True # UNCHECKED

relabeled_leslie = "NotImplementedError()" # UNCHECKED

relabeled_usher = "NotImplementedError()" # UNCHECKED

kemeny_constant = "NotImplementedError()" # UNCHECKED

hitting_times = "NotImplementedError()" # UNCHECKED

R0 = 1.19 # UNCHECKED

cohort_R0 = 1.19 # UNCHECKED

total_reproductive_output = ( # UNCHECKED
array([[1.19      , 2.67853607],
       [0.        , 0.        ]]))

T_a = 1.6694079616995068 # UNCHECKED

T_G = 1.8235294117647058 # UNCHECKED

T_R0 = 1.7426186609282468 # UNCHECKED

mu1 = 1.8235294117647058 # UNCHECKED

proba_repro = ( # FB (comparison with IBMs)
array([0.61981095, 0.66969506]))

life_expectancy_repro = 2.006160107699141 # FB (comparison with IBMs)

remaining_life_expectancy_repro = ( # FB (comparison with IBMs)
array([2.00616011, 2.33030494]))

w_G = ( # UNCHECKED
array([1., 0.]))

v_G = ( # UNCHECKED
array([1.        , 1.17647059]))

proba_maturity = "NotImplementedError()" # UNCHECKED

conditional_life_expectancy = "NotImplementedError()" # UNCHECKED

remaining_life_expectancy = ( # UNCHECKED
array([1.7, 2. ]))

life_expectancy = 1.7 # UNCHECKED

variance_remaining_lifespan = "NotImplementedError()" # UNCHECKED

variance_lifespan = "NotImplementedError()" # UNCHECKED

mean_age_class = ( # UNCHECKED
array([1.       , 2.8264801]))

mean_age_population = 1.6694079616995072 # UNCHECKED

variance_age_class = "NotImplementedError()" # UNCHECKED

variance_age_population = "NotImplementedError()" # UNCHECKED

mean_age_maturity = "NotImplementedError()" # UNCHECKED

mean_age_first_repro = "NotImplementedError()" # UNCHECKED

population_entropy = 1.6129912535470157 # UNCHECKED

birth_entropy = "NotImplementedError()" # UNCHECKED

genealogical_entropy = "NotImplementedError()" # UNCHECKED

lifetable_entropy = 0.717693243248397 # UNCHECKED

fertility_excess = 1.190000000060536 # UNCHECKED

survival_excess = 1.31666666676756 # UNCHECKED

theta = "NotImplementedError()" # UNCHECKED

survivorship = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = 0,
    output = 1.0,
  ),
  input_output( # UNCHECKED
    input = 1,
    output = 0.35,
  ),
)

class_survivorship = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = 0,
    output = array([1., 1.]),
  ),
  input_output( # UNCHECKED
    input = 1,
    output = array([0.35, 0.5 ]),
  ),
)

age_specific_fertility = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = 0,
    output = array([[0.7, 0.7],
       [0. , 0. ]]),
  ),
  input_output( # UNCHECKED
    input = 1,
    output = array([[0.7, 0.7],
       [0. , 0. ]]),
  ),
)

keyfitz_delta = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = array([1., 0.]),
    output = 0.36650164612069264,
  ),
  input_output( # UNCHECKED
    input = array([0.6334983538793074, 0.3665016461206927]),
    output = 0.0,
  ),
)

cohen_D1 = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = array([1., 0.]),
    output = 0.8178715292944982,
  ),
  input_output( # UNCHECKED
    input = array([0.6334983538793074, 0.3665016461206927]),
    output = 5.551115123125783e-17,
  ),
)

population_momentum = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = array([1., 0.]),
    output = 0.9322580645119837,
  ),
  input_output( # UNCHECKED
    input = array([0.6334983538793074, 0.3665016461206927]),
    output = 0.9925534966174214,
  ),
)
