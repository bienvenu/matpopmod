"""
Reference values for model second_order_oscillations.
Values marked as "# UNCHECKED" have not yet been checked by a human.
Otherwise, a comment indicates who checked the value and how.
"""

from collections import namedtuple
import numpy as np
nan = np.nan
inf = np.inf
array  = np.array
input_output = namedtuple("input_output","input output")
 
irreducible = True # UNCHECKED

irreducible_components = ( # UNCHECKED
[array([[0.        , 2.        , 2.        , 1.        ],
       [0.25      , 0.        , 0.        , 0.        ],
       [0.        , 0.75      , 0.        , 0.        ],
       [0.        , 0.        , 0.66666667, 0.        ]])],
[[0, 1, 2, 3]],
)

normal_form = ( # UNCHECKED
array([[0.        , 2.        , 2.        , 1.        ],
       [0.25      , 0.        , 0.        , 0.        ],
       [0.        , 0.75      , 0.        , 0.        ],
       [0.        , 0.        , 0.66666667, 0.        ]]),
array([0, 1, 2, 3]),
)

periods = [1, 1, 1, 4] # UNCHECKED

index_of_imprimitivity = 1 # UNCHECKED

aperiodic = True # UNCHECKED

primitive = True # UNCHECKED

quasi_irreducible = True # UNCHECKED

quasi_primitive = True # UNCHECKED

eigenvalues = ( # UNCHECKED
1.0000000000000002,
(-0.25000000000000033+0.4330127018922194j),
(-0.25000000000000033-0.4330127018922194j),
-0.49999999999999967,
)

right_eigenvectors = ( # UNCHECKED
array([0.64, 0.16, 0.12, 0.08]),
array([ 0.30769231-0.j        , -0.07692308-0.13323468j, -0.11538462+0.19985202j,  0.30769231-0.j        ]),
array([ 0.30769231-0.j        , -0.07692308+0.13323468j, -0.11538462-0.19985202j,  0.30769231-0.j        ]),
array([ 0.30769231, -0.15384615,  0.23076923, -0.30769231]),
)

left_eigenvectors = ( # UNCHECKED
array([0.11538462, 0.46153846, 0.30769231, 0.11538462]),
array([ 0.11080534-0.0383841j , -0.04432214+0.23030457j, -0.41367326+0.j        , -0.17728854-0.15353638j]),
array([ 0.11080534+0.0383841j , -0.04432214-0.23030457j, -0.41367326+0.j        , -0.17728854+0.15353638j]),
array([ 0.15789474, -0.31578947, -0.21052632, -0.31578947]),
)

lmbd = 1.0000000000000002 # UNCHECKED

w = ( # UNCHECKED
array([0.64, 0.16, 0.12, 0.08]))

v = ( # UNCHECKED
array([0.5952381 , 2.38095238, 1.58730159, 0.5952381 ]))

damping_ratio = 1.9999999999999996 # UNCHECKED

second_order_period = 5.999999999999999 # UNCHECKED

sensitivities = ( # UNCHECKED
array([[0.38095238, 0.0952381 , 0.07142857, 0.04761905],
       [1.52380952, 0.38095238, 0.28571429, 0.19047619],
       [1.01587302, 0.25396825, 0.19047619, 0.12698413],
       [0.38095238, 0.0952381 , 0.07142857, 0.04761905]]))

elasticities = ( # UNCHECKED
array([[0.        , 0.19047619, 0.14285714, 0.04761905],
       [0.38095238, 0.        , 0.        , 0.        ],
       [0.        , 0.19047619, 0.        , 0.        ],
       [0.        , 0.        , 0.04761905, 0.        ]]))

P = ( # UNCHECKED
array([[0.   , 0.5  , 0.375, 0.125],
       [1.   , 0.   , 0.   , 0.   ],
       [0.   , 1.   , 0.   , 0.   ],
       [0.   , 0.   , 1.   , 0.   ]]))

Ps = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

Pf = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

pi = ( # UNCHECKED
array([0.38095238, 0.38095238, 0.19047619, 0.04761905]))

entropy_rate = 0.5354815475160255 # UNCHECKED

mixed_transitions = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

fundamental_matrix = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

G = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

fundamental_matrix_Ps = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

newborn_classes = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

unique_newborn_class = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

reproductive_classes = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

postreproductive_classes = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

proportion_newborns = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

nu = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

class_of_birth = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

class_of_death = "NotImplementedError()" # UNCHECKED

leslie = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

usher = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

relabeled_leslie = "NotImplementedError()" # UNCHECKED

relabeled_usher = "NotImplementedError()" # UNCHECKED

kemeny_constant = "NotImplementedError()" # UNCHECKED

hitting_times = "NotImplementedError()" # UNCHECKED

R0 = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

cohort_R0 = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

total_reproductive_output = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

T_a = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

T_G = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

T_R0 = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

mu1 = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

proba_repro = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

life_expectancy_repro = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

remaining_life_expectancy_repro = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

w_G = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

v_G = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

proba_maturity = "NotImplementedError()" # UNCHECKED

conditional_life_expectancy = "NotImplementedError()" # UNCHECKED

remaining_life_expectancy = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

life_expectancy = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

variance_remaining_lifespan = "NotImplementedError()" # UNCHECKED

variance_lifespan = "NotImplementedError()" # UNCHECKED

mean_age_class = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

mean_age_population = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

variance_age_class = "NotImplementedError()" # UNCHECKED

variance_age_population = "NotImplementedError()" # UNCHECKED

mean_age_maturity = "NotImplementedError()" # UNCHECKED

mean_age_first_repro = "NotImplementedError()" # UNCHECKED

population_entropy = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

birth_entropy = "NotImplementedError()" # UNCHECKED

genealogical_entropy = "NotImplementedError()" # UNCHECKED

lifetable_entropy = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

fertility_excess = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

survival_excess = "NotAvailable('A = S + F decomposition required.')" # UNCHECKED

theta = "NotImplementedError()" # UNCHECKED

survivorship = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = 0,
    output = "NotAvailable('A = S + F decomposition required.')",
  ),
  input_output( # UNCHECKED
    input = 1,
    output = "NotAvailable('A = S + F decomposition required.')",
  ),
)

class_survivorship = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = 0,
    output = "NotAvailable('A = S + F decomposition required.')",
  ),
  input_output( # UNCHECKED
    input = 1,
    output = "NotAvailable('A = S + F decomposition required.')",
  ),
)

age_specific_fertility = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = 0,
    output = "NotAvailable('A = S + F decomposition required.')",
  ),
  input_output( # UNCHECKED
    input = 1,
    output = "NotAvailable('A = S + F decomposition required.')",
  ),
)

keyfitz_delta = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = array([1., 0., 0., 0.]),
    output = 0.3600000000000001,
  ),
  input_output( # UNCHECKED
    input = array([0.64               , 0.16000000000000014, 0.12000000000000001, 0.08000000000000002]),
    output = 9.020562075079397e-17,
  ),
)

cohen_D1 = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = array([1., 0., 0., 0.]),
    output = 0.5317460317460321,
  ),
  input_output( # UNCHECKED
    input = array([0.64               , 0.16000000000000014, 0.12000000000000001, 0.08000000000000002]),
    output = 2.185751579730777e-16,
  ),
)

population_momentum = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = array([1., 0., 0., 0.]),
    output = 0.595238095238095,
  ),
  input_output( # UNCHECKED
    input = array([0.64               , 0.16000000000000014, 0.12000000000000001, 0.08000000000000002]),
    output = 0.9999999999999998,
  ),
)
