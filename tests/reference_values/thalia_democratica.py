"""
Reference values for model thalia_democratica.
Values marked as "# UNCHECKED" have not yet been checked by a human.
Otherwise, a comment indicates who checked the value and how.
"""

from collections import namedtuple
import numpy as np
nan = np.nan
inf = np.inf
array  = np.array
input_output = namedtuple("input_output","input output")
 
irreducible = False # UNCHECKED

irreducible_components = ( # UNCHECKED
[array([[0.7939, 0.    , 4.233 ],
       [0.2034, 0.8162, 0.    ],
       [0.    , 0.1791, 0.7458]]), array([[0.55]])],
[[0, 2, 3], [1]],
)

normal_form = ( # UNCHECKED
array([[0.7939, 0.    , 4.233 , 0.    ],
       [0.2034, 0.8162, 0.    , 0.    ],
       [0.    , 0.1791, 0.7458, 0.    ],
       [0.1994, 0.    , 0.    , 0.55  ]]),
array([0, 2, 3, 1]),
)

periods = [1, 1, 1, 1] # UNCHECKED

index_of_imprimitivity = 1 # UNCHECKED

aperiodic = True # UNCHECKED

primitive = False # UNCHECKED

quasi_irreducible = True # UNCHECKED

quasi_primitive = True # UNCHECKED

eigenvalues = ( # UNCHECKED
1.3223395949753782,
(0.5167802025123107+0.4636961666096551j),
(0.5167802025123107-0.4636961666096551j),
0.55,
)

right_eigenvectors = ( # UNCHECKED
array([0.56026173, 0.14464646, 0.22514981, 0.069942  ]),
array([ 0.51946983-0.j        , -0.0159218 -0.2222433j , -0.10384063-0.16081268j, -0.03400788+0.05690436j]),
array([ 0.51946983-0.j        , -0.0159218 +0.2222433j , -0.10384063+0.16081268j, -0.03400788-0.05690436j]),
array([0., 1., 0., 0.]),
)

left_eigenvectors = ( # UNCHECKED
array([0.09140674, 0.        , 0.2374776 , 0.67111566]),
array([ 0.03739905-0.07572183j,  0.        +0.j        ,  0.12167111+0.18842585j, -0.69125116+0.j        ]),
array([ 0.03739905+0.07572183j,  0.        +0.j        ,  0.12167111-0.18842585j, -0.69125116+0.j        ]),
array([ 0.01878831, -0.30174528,  0.27328191, -0.4061845 ]),
)

lmbd = 1.3223395949753782 # UNCHECKED

w = ( # UNCHECKED
array([0.56026173, 0.14464646, 0.22514981, 0.069942  ]))

v = ( # UNCHECKED
array([0.60287167, 0.        , 1.56627956, 4.42633223]))

damping_ratio = 1.904518897174035 # UNCHECKED

second_order_period = 8.591686322324136 # UNCHECKED

sensitivities = ( # UNCHECKED
array([[0.33776593, 0.08720325, 0.13573644, 0.04216605],
       [0.        , 0.        , 0.        , 0.        ],
       [0.87752649, 0.22655679, 0.35264755, 0.10954872],
       [2.47990455, 0.64025329, 0.99658788, 0.30958652]]))

elasticities = ( # UNCHECKED
array([[0.20278631, 0.        , 0.        , 0.13497961],
       [0.        , 0.        , 0.        , 0.        ],
       [0.13497961, 0.        , 0.21766794, 0.        ],
       [0.        , 0.        , 0.13497961, 0.17460691]]))

P = ( # UNCHECKED
array([[0.60037528, 0.        , 0.        , 0.39962472],
       [0.58407054, 0.41592946, 0.        , 0.        ],
       [0.38276067, 0.        , 0.61723933, 0.        ],
       [0.        , 0.        , 0.43599965, 0.56400035]]))

Ps = ( # UNCHECKED
array([[0.60037528, 0.        , 0.        , 0.        ],
       [0.58407054, 0.41592946, 0.        , 0.        ],
       [0.        , 0.        , 0.61723933, 0.        ],
       [0.        , 0.        , 0.43599965, 0.56400035]]))

Pf = ( # UNCHECKED
array([[0.        , 0.        , 0.        , 0.39962472],
       [0.        , 0.        , 0.        , 0.        ],
       [0.38276067, 0.        , 0.        , 0.        ],
       [0.        , 0.        , 0.        , 0.        ]]))

pi = ( # UNCHECKED
array([0.33776593, 0.        , 0.35264755, 0.30958652]))

entropy_rate = 0.972327831371095 # UNCHECKED

mixed_transitions = False # UNCHECKED

fundamental_matrix = ( # UNCHECKED
array([[4.85201359, 0.        , 0.        , 0.        ],
       [2.14998113, 2.22222222, 0.        , 0.        ],
       [0.        , 0.        , 5.44069641, 0.        ],
       [0.        , 0.        , 3.83331521, 3.93391031]]))

G = ( # UNCHECKED
array([[ 0.        ,  0.        , 16.22642329, 16.65224233],
       [ 0.        ,  0.        ,  0.        ,  0.        ],
       [ 0.98689956,  0.        ,  0.        ,  0.        ],
       [ 0.        ,  0.        ,  0.        ,  0.        ]]))

fundamental_matrix_Ps = ( # UNCHECKED
array([[2.50234768, 0.        , 0.        , 0.        ],
       [2.50234768, 1.71212198, 0.        , 0.        ],
       [0.        , 0.        , 2.6125986 , 0.        ],
       [0.        , 0.        , 2.6125986 , 2.29357985]]))

newborn_classes = ( # UNCHECKED
array([nan,  0., nan,  0.]))

unique_newborn_class = False # UNCHECKED

reproductive_classes = ( # UNCHECKED
array([1., 0., 0., 1.]))

postreproductive_classes = ( # UNCHECKED
array([0., 1., 0., 0.]))

proportion_newborns = ( # UNCHECKED
array([0.39962472, 0.        , 0.38276067, 0.        ]))

nu = ( # UNCHECKED
array([0.72207024, 0.        , 0.27792976, 0.        ]))

class_of_birth = ( # UNCHECKED
array([[1., 1., 0., 0.],
       [0., 0., 0., 0.],
       [0., 0., 1., 1.],
       [0., 0., 0., 0.]]))

class_of_death = "NotImplementedError()" # UNCHECKED

leslie = False # UNCHECKED

usher = False # UNCHECKED

relabeled_leslie = "NotImplementedError()" # UNCHECKED

relabeled_usher = "NotImplementedError()" # UNCHECKED

kemeny_constant = "NotImplementedError()" # UNCHECKED

hitting_times = "NotImplementedError()" # UNCHECKED

R0 = 4.001730883164397 # UNCHECKED

cohort_R0 = 5.222416673531082 # UNCHECKED

total_reproductive_output = ( # UNCHECKED
array([[ 0.        ,  0.        , 16.22642329, 22.12796581],
       [ 0.        ,  0.        ,  0.        ,  0.        ],
       [ 1.29247708,  0.50897752,  0.        ,  0.        ],
       [ 0.        ,  0.        ,  0.        ,  0.        ]]))

T_a = 3.7042630607916807 # UNCHECKED

T_G = 7.1133101508117065 # UNCHECKED

T_R0 = 4.963185908524345 # UNCHECKED

mu1 = 8.757488445089738 # UNCHECKED

proba_repro = ( # FB (comparison with IBMs)
array([0.52254376, 0.        , 0.97079559, 0.99627152]))

life_expectancy_repro = 9.068137859199528 # FB (comparison with IBMs)

remaining_life_expectancy_repro = ( # FB (comparison with IBMs)
array([8.84116264,        nan, 9.38554575, 3.94484934]))

w_G = ( # UNCHECKED
array([0.80217024, 0.        , 0.19782976, 0.        ]))

v_G = ( # UNCHECKED
array([0.62330909, 0.        , 2.5274256 , 2.59375112]))

proba_maturity = "NotImplementedError()" # UNCHECKED

conditional_life_expectancy = "NotImplementedError()" # UNCHECKED

remaining_life_expectancy = ( # UNCHECKED
array([7.00199472, 2.22222222, 9.27401162, 3.93391031]))

life_expectancy = 7.633455820367238 # UNCHECKED

variance_remaining_lifespan = "NotImplementedError()" # UNCHECKED

variance_lifespan = "NotImplementedError()" # UNCHECKED

mean_age_class = ( # UNCHECKED
array([2.50234768, 4.21446966, 2.6125986 , 4.90617844]))

mean_age_population = 2.9429517651422126 # UNCHECKED

variance_age_class = "NotImplementedError()" # UNCHECKED

variance_age_population = "NotImplementedError()" # UNCHECKED

mean_age_maturity = "NotImplementedError()" # UNCHECKED

mean_age_first_repro = "NotImplementedError()" # UNCHECKED

population_entropy = 3.6017580687276296 # UNCHECKED

birth_entropy = "NotImplementedError()" # UNCHECKED

genealogical_entropy = "NotImplementedError()" # UNCHECKED

lifetable_entropy = 0.6717903342327374 # UNCHECKED

fertility_excess = 4.001730883232085 # UNCHECKED

survival_excess = 1.4834001060808077 # UNCHECKED

theta = "NotImplementedError()" # UNCHECKED

survivorship = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = 0,
    output = 1.0,
  ),
  input_output( # UNCHECKED
    input = 1,
    output = 0.993855859511829,
  ),
)

class_survivorship = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = 0,
    output = array([1., 1., 1., 1.]),
  ),
  input_output( # UNCHECKED
    input = 1,
    output = array([0.9933, 0.55  , 0.9953, 0.7458]),
  ),
)

age_specific_fertility = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = 0,
    output = array([[0.    , 0.    , 0.    , 4.233 ],
       [0.    , 0.    , 0.    , 0.    ],
       [0.2034, 0.    , 0.    , 0.    ],
       [0.    , 0.    , 0.    , 0.    ]]),
  ),
  input_output( # UNCHECKED
    input = 1,
    output = array([[0.        , 0.        , 0.76171034, 4.233     ],
       [0.        , 0.        , 0.        , 0.        ],
       [0.16256847, 0.        , 0.        , 0.        ],
       [0.        , 0.        , 0.        , 0.        ]]),
  ),
)

keyfitz_delta = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = array([1., 0., 0., 0.]),
    output = 0.43973827236544893,
  ),
  input_output( # UNCHECKED
    input = array([0.5602617276345511 , 0.14464646020626568, 0.2251498134747023 , 0.06994199868448096]),
    output = 0.0,
  ),
)

cohen_D1 = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = array([1., 0., 0., 0.]),
    output = 1.0150917926779959,
  ),
  input_output( # UNCHECKED
    input = array([0.5602617276345511 , 0.14464646020626568, 0.2251498134747023 , 0.06994199868448096]),
    output = 6.869504964868156e-16,
  ),
)

population_momentum = (# Pairs of input/output:
  input_output( # UNCHECKED
    input = array([1., 0., 0., 0.]),
    output = 0.6529403562886618,
  ),
  input_output( # UNCHECKED
    input = array([0.5602617276345511 , 0.14464646020626568, 0.2251498134747023 , 0.06994199868448096]),
    output = 1.1519549570517809,
  ),
)
