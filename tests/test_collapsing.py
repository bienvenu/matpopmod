""" Testing the matpopmod.collapsing module """
import numpy as np
import pytest
import matpopmod

def test_collapsing_matrix():
  cv = [[0,1], [2,3], [4]]
  ref = np.array([[1,1,0,0,0],
                  [0,0,1,1,0],
                  [0,0,0,0,1]])
  phi = matpopmod.collapsing.collapsing_matrix(cv)
  np.testing.assert_equal(ref, phi)

def test_collapsing_matrix_out_of_oder():
  cv = [[0,1], [4], [3,2]]
  ref = np.array([[1,1,0,0,0],
                  [0,0,0,0,1],
                  [0,0,1,1,0]])
  phi = matpopmod.collapsing.collapsing_matrix(cv)
  np.testing.assert_equal(ref, phi)

def test_collapsing_matrix_incorrect():
  cv = [[0, 1], [4], [2]]
  with pytest.raises(ValueError):
    matpopmod.collapsing.collapsing_matrix(cv)


def test_ind_collapsing_A():
  model = matpopmod.MPM(np.array([[0,   0,   8  ],
                                  [0.8, 0,   0  ],
                                  [0,   0.7, 0.5]]))
  cv = [[0,1],[2]]
  collapsed = matpopmod.collapsing.individualistic_collapsing(model, cv)
  np.testing.assert_almost_equal(model.lmbd, collapsed.lmbd)
  np.testing.assert_almost_equal([model.w[:2].sum(), model.w[2]], collapsed.w)


def test_ind_collapsing_SF():
  model = matpopmod.MPM(S=np.array([[0,   0,   0  ],
                                    [0.8, 0,   0  ],
                                    [0,   0.7, 0.5]]),
                        F=np.array([[0,   0,   8  ],
                                    [0,   0,   0  ],
                                    [0,   0,   0]]),)
  cv = [[0,1],[2]]
  collapsed = matpopmod.collapsing.individualistic_collapsing(model, cv)
  np.testing.assert_almost_equal(model.lmbd, collapsed.lmbd)
  np.testing.assert_almost_equal([model.w[:2].sum(), model.w[2]], collapsed.w)


def test_gene_collapsing_SF():
  model = matpopmod.MPM(S=np.array([[0,   0,   0  ],
                                    [0.2, 0,   0  ],
                                    [0,   0.2, 0.2]]),
                        F=np.array([[0,   0,   8  ],
                                    [0,   0,   0  ],
                                    [0,   0,   0]]),)
  cv = [[0],[1, 2]]
  collapsed = matpopmod.collapsing.genealogical_collapsing(model, cv)
  assert collapsed.split
  np.testing.assert_almost_equal(model.lmbd, collapsed.lmbd,
                                 err_msg="lambda not preserved")
  np.testing.assert_almost_equal([model.w[0], model.w[1:].sum()], collapsed.w,
                                 err_msg="Stable proportions not preserved")
  np.testing.assert_almost_equal(model.T_a, collapsed.T_a,
                                 err_msg="T_a not preserved")
  np.testing.assert_almost_equal([(model.v[0]*model.w[0])/model.w[0],
                                  model.v[1:].dot(model.w[1:])/model.w[1:].sum()],
                                 collapsed.v,
                                 err_msg="Reproductive values not preserved")
  e = model.elasticities
  np.testing.assert_almost_equal([[e[0,0], e[0,1] + e[0,2] ],
                                  [e[1,0] + e[2,0], e[1,1]+e[2,2]+e[1,2]+e[2,1]]],
                                 collapsed.elasticities,
                                 err_msg="Elasticities not preserved")

def test_gene_collapsing_SF_nosplit():
  model = matpopmod.MPM(S=np.array([[0,   0,   0  ],
                                    [0.9, 0,   0  ],
                                    [0,   0.8, 0.9]]),
                        F=np.array([[0,   0,   8  ],
                                    [0,   0,   0  ],
                                    [0,   0,   0]]),)
  cv = [[0],[1, 2]]
  with pytest.warns(UserWarning, match="Genealogical collapsing yields"
                    " survival probabilities > 1"):
    collapsed = matpopmod.collapsing.genealogical_collapsing(model, cv)
  assert not collapsed.split
  np.testing.assert_almost_equal(model.lmbd, collapsed.lmbd,
                                 err_msg="lambda not preserved")
  np.testing.assert_almost_equal([model.w[0], model.w[1:].sum()], collapsed.w,
                                 err_msg="Stable proportions not preserved")
  np.testing.assert_almost_equal([(model.v[0]*model.w[0])/model.w[0],
                                  model.v[1:].dot(model.w[1:])/model.w[1:].sum()],
                                 collapsed.v,
                                 err_msg="Reproductive values not preserved")
  e = model.elasticities
  np.testing.assert_almost_equal([[e[0,0], e[0,1] + e[0,2]],
                                  [e[1,0] + e[2,0], e[1,1]+e[2,2]+e[1,2]+e[2,1]]],
                                 collapsed.elasticities,
                                 err_msg="Elasticities not preserved")

def test_collapsing_incorrect():
  model = matpopmod.MPM(np.array([[0,   0,   8  ],
                                  [0.8, 0,   0  ],
                                  [0,   0.7, 0.5]]))
  cv = [[0,1],[2, 3]]
  with pytest.raises(ValueError):
    matpopmod.collapsing.individualistic_collapsing(model, cv)
  with pytest.raises(ValueError):
    matpopmod.collapsing.genealogical_collapsing(model, cv)
