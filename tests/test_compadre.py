import os
import matpopmod.compadre
import numpy as np
import matpopmod as mpm
import pytest

EXTRACT = """
{"metadata": [
  {"MatrixID": 1, "name":"test_matrix", "MatrixSplit":"Divided"},
  {"MatrixID": 2, "name":"test_matrix2", "MatrixSplit":"Indivisible"},
  {"MatrixID": 3, "name":"test_matrix_error", "MatrixSplit":"Indivisible"}
],
 "matrixClass": [
  [
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 1,   "MatrixID": 1},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 2,   "MatrixID": 1},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 3,   "MatrixID": 1},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 4,   "MatrixID": 1},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 5,   "MatrixID": 1},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 6,   "MatrixID": 1},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 7,   "MatrixID": 1},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 8,   "MatrixID": 1},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 9,   "MatrixID": 1}],
    [
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 1,   "MatrixID": 2},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 2,   "MatrixID": 2},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 3,   "MatrixID": 2},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 4,   "MatrixID": 2},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 5,   "MatrixID": 2},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 6,   "MatrixID": 2},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 7,   "MatrixID": 2},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 8,   "MatrixID": 2},
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 9,   "MatrixID": 2}],
  [
{"MatrixClassOrganized": "active",  "MatrixClassAuthor": "test",  "MatrixClassNumber": 1,   "MatrixID": 3}]
],
 "mat": [{"matA": [[0.077, 0, 0, 0, 0, 0, 0, 0, 0],
   [0.037, 0, 0, 0, 0, 0, 0, 0, 0],
   [0.003, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0]],
  "matU": [[0.077, 0, 0, 0, 0, 0, 0, 0, 0],
   [0.037, 0, 0, 0, 0, 0, 0, 0, 0],
   [0.003, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0]],
  "matF": [[0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0]],
  "matC": [[0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0]],
  "MatrixID": [1]},
{"matA": [[0.077, 0, 0, 0, 0, 0, 0, 0, 0],
   [0.037, 0, 0, 0, 0, 0, 0, 0, 0],
   [0.003, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0]],
  "matU": [[0.077, 0, 0, 0, 0, 0, 0, 0, 0],
   [0.037, 0, 0, 0, 0, 0, 0, 0, 0],
   [0.003, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0]],
  "matF": [[0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0]],
  "matC": [[0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0],
   [0, 0, 0, 0, 0, 0, 0, 0, 0]],
  "MatrixID": [2]},
   {}],
 "version": {"Database": ["COMPADRE"],
  "Version": ["test"],
  "Type": ["test"]}}
"""

def test_compadre_loading(tmp_path):
  fpath = os.path.join(tmp_path,"compadre_test.json")
  with open(fpath, 'w') as f:
    f.write(EXTRACT)
  db = matpopmod.compadre.load(fpath)
  print(db)
  print(db.info)
  assert db[0].metadata["name"] == "test_matrix"
  assert db[1].metadata["name"] == "test_matrix2"
  assert db.invalid_models[0].metadata["name"] == "test_matrix_error"

def test_compadre_loading_error(tmp_path):
  fpath = os.path.join(tmp_path,"compadre_test.json")
  with open(fpath, 'w') as f:
    f.write('''{"metadata":[0],
    "version":[],
    "mat":[],
    "matrixClass":[]}''')
  with pytest.raises(matpopmod.compadre.CompadreError):
    matpopmod.compadre.load(fpath)


def test_save_and_reload(tmp_path):
  fpath = os.path.join(tmp_path, "examples.json")
  not_split = mpm.MPM(A = np.zeros((3,3)))
  examples = mpm.compadre.MPMCollection(mpm.examples.all_models+[not_split])
  print(examples)
  N = len(examples)
  mpm.compadre.save(examples, fpath)
  db = mpm.compadre.load(fpath)
  print(db)
  assert len(db) == N
  for m1, m2 in zip(db, examples):
    assert m1.dim == m2.dim
    np.testing.assert_allclose(m1.A, m2.A)
  for m in db.invalid_models:
    print(m.error)
    print(m)
    print('---')

def test_filter():
  invalid = mpm.compadre.InvalidMPM()
  invalid2 = mpm.compadre.InvalidMPM(A=np.zeros((2,2)))
  collection = mpm.compadre.MPMCollection(mpm.examples.all_models,
                                          invalid_models=[invalid, invalid2])
  collection.info["status"] = 'to_filter'

  filtered = mpm.compadre.filter_collection(lambda m: m.dim==2, collection)
  assert any([m.dim > 2 for m in collection])
  assert all([m.dim == 2 for m in filtered])
  assert len(filtered.invalid_models) == 0

  filtered_winvalid = mpm.compadre.filter_collection(lambda m: m.dim==2, collection,
                                                     include_invalid=True)
  assert len(filtered_winvalid.invalid_models) == 1

  filtered_without_info = mpm.compadre.filter_collection(lambda m: m.dim==2, collection,
                                                         copy_info=False)
  assert filtered.info == collection.info
  assert len(filtered_without_info.info) == 0

def test_merge():
  invalid1 = mpm.compadre.InvalidMPM()
  invalid2 = mpm.compadre.InvalidMPM()

  part1 = mpm.compadre.MPMCollection(mpm.examples.all_models[:3], invalid_models=[invalid1])
  part2 = mpm.compadre.MPMCollection(mpm.examples.all_models[3:], invalid_models=[invalid2])

  part1.info["in_both"] = "Hello"
  part2.info["in_both"] = "World"
  part1.info["in_1"] = "Hello World"
  part2.info["in_2"] = "Hello World"
  merged = mpm.compadre.merge(part1, part2)
  print(merged.info)
  assert len(part1)+len(part2) == len(merged)
  assert merged.info["in_1"] == 'Hello World'
  assert merged.info["in_2"] == 'Hello World'
  assert merged.info["in_both"] == ('Hello', 'World')
  assert len(merged.invalid_models) == 0
  merged_noinfo = mpm.compadre.merge(part1, part2, copy_info=False)
  assert len(merged_noinfo.info) == 0
  merged_withinvalid = mpm.compadre.merge(part1, part2, include_invalid=True)
  assert len(merged_withinvalid.invalid_models) == 2

def test_get_from_id():
  a = mpm.MPM(A=np.zeros((2,2))+0.1, metadata={"MatrixID":1})
  b = mpm.MPM(A=np.zeros((2,2))+0.2, metadata={"MatrixID":2})
  c = mpm.InvalidMPM(metadata={"MatrixID":3})
  collection = mpm.compadre.MPMCollection([a,b], invalid_models=[c])
  assert collection.get_from_id(1) == a
  assert collection.get_from_id(2) == b
  assert collection.get_from_id(3) == c
  with pytest.raises(KeyError):
    collection.get_from_id(4)
