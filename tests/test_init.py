''' Test instanciation rules of matpopmod.model '''

from collections import namedtuple
import os
import pathlib
import numpy as np
import numpy.random
import pytest
import matpopmod.utils as ut
from matpopmod.model import MPM

Case = namedtuple("TestCase",
                  ["A","F","S", "exception","name"],
                  defaults=(None, None, None, Exception, "test"))

MATRIX = np.array([[0.1,0.3],[0.4,0.6]])
EYE3 = np.eye(3)
NOT_SQUARE = numpy.random.random(size=(3,2))
NOT_2D = numpy.random.random(size=(2,2,2))
MATRIX_ABOVE1 = numpy.random.random(size=(2,2)) + 1
NEGATIVE = MATRIX - 2*np.eye(2)
NAN_MATRIX = np.array([[0.   , 0.  ],
                       [0.14,  np.nan]])
STR_MATRIX = "0.1, 0.3; 0.4, 0.6"
STR_INC_MATRIX = "0.1, 2; 0.4, 0.8;"
NPTEXT_MATRIX = """
# Mock matrix file
0.1 0.3
0.4 0.6"""


CORRECT = [
    Case(A=MATRIX, name="A/numpy"),
    Case(A=MATRIX, F=MATRIX, name="A-F/numpy"),
    Case(A=MATRIX, S=MATRIX, name="A-S/numpy"),
    Case(F=MATRIX, S=MATRIX, name="S-F/numpy"),
    Case(A=STR_MATRIX, name="A/str"),
    Case(A=STR_MATRIX, F=STR_MATRIX, name="A-F/str"),
    Case(A=STR_MATRIX, S=STR_MATRIX, name="A-S/str"),
    Case(F=STR_MATRIX, S=STR_MATRIX, name="S-F/str"),
    ]

INCORRECT = [
  Case(A=None, F=None, S=None, exception=ut.MissingArguments, name='No matrix'),
  Case(S=MATRIX, exception=ut.MissingArguments, name="Missing F"),
  Case(F=MATRIX, exception=ut.MissingArguments, name="Missing S"),
  Case(A=NOT_SQUARE, exception=ut.IncorrectDims,name="Non-square A"),
  Case(S=MATRIX, F=NOT_SQUARE, exception=ut.IncorrectDims, name="Non-square F"),
  Case(F=MATRIX, S=NOT_SQUARE, exception=ut.IncorrectDims, name="Non-square S"),
  Case(F=MATRIX, S=EYE3, exception=ut.IncorrectDims, name="Different dim"),
  Case(F=MATRIX, S=NOT_2D, exception=ut.IncorrectDims, name="Non 2D S"),
  Case(S=MATRIX, F=NOT_2D, exception=ut.IncorrectDims, name="Non 2D F"),
  Case(A=NOT_2D, exception=ut.IncorrectDims, name="Non 2D A"),
  Case(A=NAN_MATRIX, exception=ut.MissingEntries, name='NaN in A'),
  Case(S=NAN_MATRIX, F=NAN_MATRIX, exception=ut.MissingEntries, name='NaN is S,F'),
  Case(S=STR_INC_MATRIX, exception=ValueError, name='Incorrect str-matrix'),
  Case(A=NEGATIVE, exception=ut.NegativeEntries, name='Negative entries in A'),
  Case(F=MATRIX, S=MATRIX_ABOVE1, exception=ut.InadequateMatrix, name='S non convergent'),
]

@pytest.mark.parametrize("A, F, S, exception, _", CORRECT,
                         ids=[x.name for x in CORRECT])
def test_correct_matrice(A, F, S, exception, _):
  """ Check if correct input matrices do not raise exception"""
  MPM(A=A,F=F,S=S)

@pytest.mark.parametrize("A, F, S, exception, _", INCORRECT,
                         ids=[x.name for x in INCORRECT])
def test_incorrect_matrice(A, F, S, exception, _):
  """ Check if incorrect input matrices raise the correct exception """
  with pytest.raises(exception):
    MPM(A=A,F=F,S=S)

def test_missing_matrices_exception():
  m = MPM(A=MATRIX)
  with pytest.raises(ut.NotAvailable):
    print(m.F)
  with pytest.raises(ut.NotAvailable):
    print(m.S)


def test_file_loading(tmp_path):
  fpath = pathlib.Path(os.path.join(tmp_path,"matrix"))
  with open(fpath, 'w') as f:
    f.write(NPTEXT_MATRIX)

  m = MPM(A=fpath)
  print(m.A)
  np.testing.assert_allclose(m.A, MATRIX)
