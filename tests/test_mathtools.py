"""Test mathtools functions with simple matrices"""
import numpy as np
from numpy.testing import assert_almost_equal
import pytest

import matpopmod.mathtools as mt
import matpopmod.utils as ut

NON_IRREDUCIBLE = np.array([[1,1,0],
                  [1,1,0],
                  [0,0,1]])
NON_IRREDUCIBLE2 = np.array([[1,1,0],
                  [1,1,0],
                  [1,0,1]])
NON_IRREDUCIBLE3 = np.array([[1,1,0],
                  [1,1,1],
                  [0,0,1]])
PERIODIC = np.array([[0,1,0],
                     [0,0,1],
                     [1,0,0]])
PRIMITIVE = np.array([[1,1,0],
                      [1,1,1],
                      [1,0,1]])

TWO_PERIODS = np.array([[0,1,0,0,0],
                        [1,0,0,0,0],
                        [0,0,0,1,0],
                        [0,0,0,0,1],
                        [0,0,1,0,0]])
M = np.array([[3,-6],
              [2,-5]])

NOT_SUBSTOCHASTIC1 = np.array([[0.5,0.5],
                          [0.6,0.5]])
NOT_SUBSTOCHASTIC2 = np.array([[0.5,0.6],
                          [0.5,0.4]])
SUBSTOCHASTIC = np.array([[0.5,0.5],
                          [0.5,0.4]])

NILPOTENT = np.array([[0,1],
                      [0,0]])

E = np.eye(3)

def test_are_linearly_independent():
  assert mt.are_linearly_independent([])
  assert mt.are_linearly_independent([E[0], E[1]])

def test_not_linearly_independant():
  assert not mt.are_linearly_independent([E[0],E[0]]), "rank<m"
  assert not mt.are_linearly_independent([E[0]]*(E.shape[0]+1)), "m>n"

def test_linearly_independant_exceptions():
  with pytest.raises(ValueError):
    assert not mt.are_linearly_independent([E[0],E]), "matrix"
  with pytest.raises(ut.IncorrectDims):
    assert not mt.are_linearly_independent([E[0],np.array([1])]), "dim"

def test_primitive_underflow():
  matrix = np.array([[1e-5]])
  assert mt.is_primitive(matrix)

def test_irreducible_1d():
  assert not mt.is_irreducible(np.array([[0]]))
  assert mt.is_irreducible(np.array([[1]]))

def test_non_primitive_because_non_irreducible():
  assert not mt.is_primitive(NON_IRREDUCIBLE)

def test_non_primitive_because_periodic():
  assert not mt.is_primitive(PERIODIC)

def test_primitive():
  assert mt.is_primitive(PRIMITIVE)


@pytest.mark.parametrize("matrix", (NOT_SUBSTOCHASTIC1, NOT_SUBSTOCHASTIC2))
def test_not_substochastic(matrix):
  assert not mt.is_substochastic(matrix)
  with pytest.raises(ut.InadequateMatrix):
    ut.assert_substochastic(matrix)

def test_substochastic():
  assert mt.is_substochastic(SUBSTOCHASTIC)
  ut.assert_substochastic(SUBSTOCHASTIC)

def test_non_irreducible():
  assert not mt.is_irreducible(NON_IRREDUCIBLE)
  assert not mt.is_irreducible(NON_IRREDUCIBLE2)
  assert not mt.is_irreducible(NON_IRREDUCIBLE3)
  assert not mt.is_irreducible(TWO_PERIODS)

def test_irreducible():
  assert mt.is_irreducible(PRIMITIVE)

def test_strongly_connected_components():
  components = mt.strongly_connected_components(NON_IRREDUCIBLE)
  assert components[1] == [0,1]
  assert components[0] == [2]

def test_strongly_connected_components_topo_order():
  components = mt.strongly_connected_components(NON_IRREDUCIBLE2)
  assert components[0] == [0,1]
  assert components[1] == [2]
  components = mt.strongly_connected_components(NON_IRREDUCIBLE3)
  assert components[1] == [0,1]
  assert components[0] == [2]

def test_periods():
  period = mt.periods(PERIODIC)
  print(period)
  assert period == [3,3,3]
  period = mt.periods(TWO_PERIODS)
  print(period)
  assert period == [2,2,3,3,3]

def test_index_of_imprimitivity_irreducible():
  idx = mt.index_of_imprimitivity(PERIODIC)
  assert idx == 3

def test_index_of_imprimitivity_reducible():
  idx = mt.index_of_imprimitivity(NON_IRREDUCIBLE)
  assert idx == 1

def test_index_of_imprimitivity_irreducible_twop():
  idx = mt.index_of_imprimitivity(TWO_PERIODS)
  assert idx == 6, "Least common multpiple of 2 and 3"

def test_index_of_imprimitivity_nilpotent():
  with pytest.warns(UserWarning, match="Matrix has no cycle"):
    idx = mt.index_of_imprimitivity(NILPOTENT)
  assert np.isnan(idx)

def test_eigenelements():
  val, vect = mt.eigen_elements(M, left=False)
  print(val)
  print(vect)
  assert (val == np.array([1,-3])).all()
  for v, ref in zip(vect, (np.array([.75, .25]),
                           np.array([.5, .5]))):
    assert (v==ref).all()

def test_geometric_multiplicity():
  assert mt.geometric_multiplicity(M, -3) == 1
  assert mt.geometric_multiplicity(M, 1) == 1
  assert mt.geometric_multiplicity(M, 3) == 0
  assert mt.geometric_multiplicity(M, 0) == 0

def test_spectral_radius():
  assert mt.spectral_radius(M) == 3

def test_shannon_wrong_array():
  with pytest.raises(ValueError):
    mt.shannon_entropy(np.array([-0.3, 0.7, 0.3]))
  with pytest.raises(ValueError):
    mt.shannon_entropy(np.array([.3,.6]))

def test_shannon_entropy():
  assert_almost_equal(mt.shannon_entropy(np.array([.5,.5])),
                      1)
  assert_almost_equal(mt.shannon_entropy(np.array([5/12,5/12,2/12])),
                      1.4833557)
def test_student_t():
  assert mt.student_t(31) == 2
  assert mt.student_t(1) == 12.71
  with pytest.raises(ValueError):
    mt.student_t(-3)
