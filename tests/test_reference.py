"""test_known_mpm:  Performs comparison tests

For a handful of known models, the numerical values of descriptors are
checked against the ones that were computed (once and for all) and 
put in /tests/reference_values. 
"""

from collections import namedtuple
import matpopmod.model
import matpopmod.utils
import matpopmod.examples 
import pytest
import numpy
import reference_values
from reference_values import *

input_output = namedtuple("in_out","input output")

RTOL = 1e-5
ATOL = 1e-8

FIELDS = [
  'irreducible',
  'irreducible_components',
  'normal_form',
  'periods',
  'index_of_imprimitivity',
  'aperiodic',
  'primitive',
  'quasi_irreducible',
  'quasi_primitive',
  'eigenvalues',
  'right_eigenvectors',
  'left_eigenvectors',
  'lmbd',
  'w',
  'v',
  'damping_ratio',
  'second_order_period',
  'sensitivities',
  'elasticities',
  'P',
  'Ps',
  'Pf',
  'pi',
  'entropy_rate',
  'mixed_transitions',
  'fundamental_matrix',
  'G',
  'fundamental_matrix_Ps',
  'newborn_classes',
  'unique_newborn_class',
  'reproductive_classes',
  'postreproductive_classes',
  'proportion_newborns',
  'nu',
  'class_of_birth',
  'class_of_death',
  'leslie',
  'usher',
  'relabeled_leslie',
  'relabeled_usher',
  'kemeny_constant',
  'hitting_times',
  'R0',
  'cohort_R0',
  'total_reproductive_output',
  'T_a',
  'T_G',
  'T_R0',
  'mu1',
  'proba_repro',
  'life_expectancy_repro',
  'remaining_life_expectancy_repro',
  'w_G',
  'v_G',
  'proba_maturity',
  'conditional_life_expectancy',
  'remaining_life_expectancy',
  'life_expectancy',
  'variance_remaining_lifespan',
  'variance_lifespan',
  'mean_age_class',
  'mean_age_population',
  'variance_age_class',
  'variance_age_population',
  'mean_age_maturity',
  'mean_age_first_repro',
  'population_entropy',
  'birth_entropy',
  'genealogical_entropy',
  'lifetable_entropy',
  'fertility_excess',
  'survival_excess',
  'theta'
]
  
FIELDS_n0 =[
  "keyfitz_delta",
  "cohen_D1",
  "population_momentum",
]

FIELDS_t = [
  "survivorship",
  "class_survivorship",
  "age_specific_fertility"
]

MODELS = matpopmod.examples.all_models

def assert_equal(reference_data, computed_value):
  """ Assert that the computed value is the same than the reference value"""
  print("Expected:")
  print(reference_data)
  print("Computed:")
  print(computed_value)
  if isinstance(computed_value, tuple):
    for ref,comp in zip(reference_data, computed_value):
      try:
        numpy.testing.assert_allclose(comp, ref,
                                      rtol=RTOL, atol=ATOL)
      except TypeError:
        numpy.testing.assert_equal(comp, ref)
  else:
    try:
      numpy.testing.assert_allclose(computed_value,
                                    reference_data,
                                    rtol=RTOL, atol=ATOL)
    except TypeError:
      numpy.testing.assert_equal(computed_value,
                                 reference_data)

def is_input_output(x):
  """Test if an object is an input/output pair"""
  return hasattr(x, '_fields') and 'input' in x._fields and 'output' in x._fields
  
@pytest.mark.parametrize("model", MODELS, ids=[model.metadata['ModelName'] for model in MODELS])
@pytest.mark.parametrize("field", FIELDS+FIELDS_n0+FIELDS_t)
def test_known_values(model, field):
  try:
    reference_module = getattr(reference_values, model.metadata['ModelName'])
    reference_data = getattr(reference_module, field)
  except NameError as ex:
    raise NameError("No reference data") from ex

  if (isinstance(reference_data, tuple) and
      all([is_input_output(x) for x in reference_data])):
    for n,entry in enumerate(reference_data):
      print("~"*10+" Entry {}/{}".format(n+1, len(reference_data)) +" " + "~"*10)
      print("input:\n{}".format(entry.input))
      print("output:\n{}".format(entry.output))
      try:
        computed_value = getattr(model, field)(entry.input)
      except NotImplementedError:
        pytest.skip("Not Implemented")
      except (matpopmod.utils.NotAvailable,
              matpopmod.utils.MissingEntries) as ex:
        computed_value = "{}".format(ex.__repr__())
      assert_equal(entry.output, computed_value)
      print("~ OK")
  else:
    try:
      computed_value = getattr(model, field)
    except NotImplementedError:
      pytest.skip("Not Implemented")
    except matpopmod.utils.NotAvailable as ex:
      computed_value = "{}".format(ex.__repr__())
    assert_equal(reference_data, computed_value)


def test_attributes_readonly():
  model = matpopmod.examples.dipsacus_sylvestris
  writeable = []
  for f in FIELDS+FIELDS_n0+FIELDS_t:
    try:
      value = getattr(model, f)
    except (NotImplementedError):
      pass
    if isinstance(value, numpy.ndarray):
      print("!! " if value.flags.writeable else "OK " , f)
      if value.flags.writeable:
        writeable.append(f)
    else:
      try:
        for i, v in enumerate(value):
          if isinstance(v, numpy.ndarray):
            print("!! " if v.flags.writeable else "OK " , f, "entry {}".format(i))
            if v.flags.writeable:
              writeable.append("{} entry {}".format(f, i))
      except TypeError:
        pass
  print("{} attributes are writeable".format(len(writeable)))
  assert not len(writeable), "Some attributes are not read-only"
