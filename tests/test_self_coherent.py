""" self-coherence tests: elementary mathematical properties

  - *A.w* = *lmbd.w* and *v.A* = *lmbd.v*
  - *w* > 0 and *v* > 0
  - *pi.P* = *pi*
  - row-sum(*E*) = column-sum(*E*) = *pi* and sum(*E*) = 1
  - *damping_ratio* > 1
  - *T* >= 1
  - *N (I - S)* = *I*
  - *G* >= 0
  - *G.w_G* = *R0.w_G* and *v_G.G* = *R0.v_G*
  - *w_G* >= 0 and *v_G* >= 0
  - *mu1* > 0.

"""

import pytest
import numpy as np
import numpy.testing

import matpopmod.model

PRIMITIVE_MODELS = [m for m in matpopmod.examples.all_models if m.quasi_primitive]
IMPRIMITIVE_MODELS = [m for m in matpopmod.examples.all_models if not m.quasi_primitive]
SPLIT_PRIMITIVE_MODELS = [m for m in PRIMITIVE_MODELS if m.split]
IDS = [m.metadata['ModelName'] for m in PRIMITIVE_MODELS]
N_IDS = [m.metadata['ModelName'] for m in IMPRIMITIVE_MODELS]
S_IDS = [m.metadata['ModelName'] for m in SPLIT_PRIMITIVE_MODELS]

@pytest.mark.parametrize("model", IMPRIMITIVE_MODELS, ids=N_IDS)
def test_imprimitive_eigenvector(model):
  with pytest.warns(UserWarning, match="A is not quasi-primitive"):
    print("w\n", model.w)
  assert np.isnan(model.w).all()

  with pytest.warns(UserWarning, match="A is not quasi-primitive"):
    print("v\n", model.v)
  assert np.isnan(model.v).all()
 
@pytest.mark.parametrize("model", PRIMITIVE_MODELS, ids=IDS)
def test_right_eigenvector(model):
  numpy.testing.assert_allclose(model.A @ model.w,
                                model.lmbd * model.w)

@pytest.mark.parametrize("model", PRIMITIVE_MODELS, ids=IDS)
def test_left_eigenvector(model):
  numpy.testing.assert_allclose(model.A.T @ model.v,
                                model.lmbd * model.v)


@pytest.mark.parametrize("model", SPLIT_PRIMITIVE_MODELS, ids=S_IDS)
def test_right_eigenvector_of_g(model):
  numpy.testing.assert_allclose(model.G @ model.w_G,
                                model.R0 * model.w_G)

@pytest.mark.parametrize("model", SPLIT_PRIMITIVE_MODELS, ids=S_IDS)
def test_left_eigenvector_of_g(model):
  numpy.testing.assert_allclose(model.G.T @ model.v_G,
                                model.R0 * model.v_G)

@pytest.mark.parametrize("model", PRIMITIVE_MODELS, ids=IDS)
def test_right_eigenvector_positive(model):
  assert (model.v>=0).all()

@pytest.mark.parametrize("model", PRIMITIVE_MODELS, ids=IDS)
def test_left_eigenvector_positive(model):
  assert (model.w>=0).all()

@pytest.mark.parametrize("model", SPLIT_PRIMITIVE_MODELS, ids=S_IDS)
def test_right_eigenvector_of_g_positive(model):
  assert (model.v_G>=0).all()

@pytest.mark.parametrize("model", SPLIT_PRIMITIVE_MODELS, ids=S_IDS)
def test_left_eigenvector_of_g_positive(model):
  assert (model.w_G>=0).all()

@pytest.mark.parametrize("model", SPLIT_PRIMITIVE_MODELS, ids=S_IDS)
def test_g_matrix_positive(model):
  assert (model.G>=0).all()

@pytest.mark.parametrize("model", SPLIT_PRIMITIVE_MODELS, ids=S_IDS)
def test_mu1_positive(model):
  assert model.mu1>0

@pytest.mark.parametrize("model", PRIMITIVE_MODELS, ids=IDS)
def test_damping_ratio(model):
  assert model.damping_ratio >= 1

@pytest.mark.parametrize("model", SPLIT_PRIMITIVE_MODELS, ids=S_IDS)
def test_generation_time(model):
  assert model.T_a >= 1 - 1e-9

@pytest.mark.parametrize("model", PRIMITIVE_MODELS, ids=IDS)
def test_backward_genealogical_matrix(model):
  numpy.testing.assert_allclose(model.P.T @ model.pi,
                                model.pi)

@pytest.mark.parametrize("model", PRIMITIVE_MODELS, ids=IDS)
def test_elasticities(model):
  numpy.testing.assert_allclose(np.sum(model.elasticities, axis = 0),
                                model.pi)
  numpy.testing.assert_allclose(np.sum(model.elasticities, axis = 1),
                                model.pi)
  numpy.testing.assert_allclose(np.sum(model.elasticities), 1.)
